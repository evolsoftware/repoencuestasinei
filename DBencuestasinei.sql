CREATE DATABASE  IF NOT EXISTS `encuestasinei` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `encuestasinei`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: encuestasinei
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `encuesta`
--

DROP TABLE IF EXISTS `encuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuesta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `Descripcion` varchar(50) NOT NULL,
  `Fecha` date NOT NULL,
  `Costo` decimal(5,2) NOT NULL,
  `AlcanceNacional` tinyint(1) NOT NULL,
  `Estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuesta`
--

LOCK TABLES `encuesta` WRITE;
/*!40000 ALTER TABLE `encuesta` DISABLE KEYS */;
INSERT INTO `encuesta` VALUES (1,'Datos personales','Informacion personal del encuestado','2017-11-20',100.00,1,1),(2,'Informacion familiar','Informacion de la familia del encuestado','2017-11-05',90.00,1,1),(3,'Detalles de vivienda','informacion de la vivienda del encuestado','2017-11-02',120.00,0,1),(4,'Informacion laboral','Detalles de la ocupacion del encuestado','2017-11-17',130.00,0,0);
/*!40000 ALTER TABLE `encuesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pregunta`
--

DROP TABLE IF EXISTS `pregunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pregunta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `Orden` int(11) NOT NULL,
  `TipoPreguntaID` int(11) NOT NULL,
  `EncuestaID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Pregunta_Encuesta` (`EncuestaID`),
  KEY `FK_Pregunta_TipoPregunta` (`TipoPreguntaID`),
  CONSTRAINT `FK_Pregunta_Encuesta` FOREIGN KEY (`EncuestaID`) REFERENCES `encuesta` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Pregunta_TipoPregunta` FOREIGN KEY (`TipoPreguntaID`) REFERENCES `tipopregunta` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pregunta`
--

LOCK TABLES `pregunta` WRITE;
/*!40000 ALTER TABLE `pregunta` DISABLE KEYS */;
INSERT INTO `pregunta` VALUES (1,'¿En que ciudad nacio?',1,1,1),(2,'¿Radica usted en Peru?',4,3,1),(3,'¿En que año nació?',2,2,1),(4,'¿Se encuentra afiliado a algún seguro?',10,3,1),(5,'¿Cuantos idiomas domina usted?',3,2,1),(6,'¿Con que etnia se siente usted identificado?',6,1,1),(7,'¿Cual es el numero de su DNI?',8,2,1),(8,'¿Padece alguna enfermedad permanente?',9,3,1),(9,'¿Suele viajar al exterior del pais?',5,3,1),(10,'¿Cual es su religión?',7,1,1),(11,'¿Con cuantas personas vive?',1,2,2),(12,'¿Es usted jefe de familia?',2,3,2),(13,'¿Tiene familiares en el extranjero?',5,3,2),(14,'¿En que ciudad nació su madre?',3,1,2),(15,'¿En que ciudad nació su padre?',4,1,2),(16,'¿Cuantos hermanos(as) tiene?',7,2,2),(17,'¿Tiene familiares en provincia?',6,3,2),(18,'¿Cuantos hijos(as) vivos tiene?',9,2,2),(19,'¿Es usted casado?',8,3,2),(20,'¿Cuantos idiomas hablan en su familia?',10,2,2),(21,'¿Cuantas viviendas posee?',1,2,3),(22,'¿Cuantos años vive en su actual hogar?',2,2,3),(24,'¿De que material son las paredes de su hogar?',4,1,3),(25,'¿Posee servicio electrico?',7,3,3),(26,'¿Posee servicio de agua potable?',8,3,3),(27,'¿En que distrito vive actualmente?',3,1,3),(28,'¿De que material son los techos de su hogar?',5,1,3),(29,'¿De que material son los pisos de su hogar?',6,1,3),(30,'¿Cuantas habitaciones tiene la vivienda?',9,2,3),(31,'¿Que energía o combustible utiliza al cocinar?',10,1,3),(32,'¿Que ocupación desempeña actualmente?',1,1,4),(33,'¿Estuvo buscando trabajo últimamente?',3,3,4),(34,'¿Hace cuantos años realiza esta labor?',2,2,4),(35,'¿En que distrito se ubica su centro laboral?',4,1,4),(36,'¿Trabaja en una empresa?',7,3,4),(37,'¿Tienes un negocio propio?',5,3,4),(39,'¿Cuantas horas labora?',6,2,4),(40,'¿Cuantas horas le toma llegar a su trabajo?',8,2,4);
/*!40000 ALTER TABLE `pregunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respuesta`
--

DROP TABLE IF EXISTS `respuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `respuesta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario_EncuestaID` int(11) NOT NULL,
  `PreguntaID` int(11) NOT NULL,
  `Texto` varchar(150) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Respuesta_Pregunta` (`PreguntaID`),
  KEY `FK_Respuesta_Usuario_Encuesta` (`Usuario_EncuestaID`),
  CONSTRAINT `FK_Respuesta_Pregunta` FOREIGN KEY (`PreguntaID`) REFERENCES `pregunta` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Respuesta_Usuario_Encuesta` FOREIGN KEY (`Usuario_EncuestaID`) REFERENCES `usuario_encuesta` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respuesta`
--

LOCK TABLES `respuesta` WRITE;
/*!40000 ALTER TABLE `respuesta` DISABLE KEYS */;
INSERT INTO `respuesta` VALUES (1,1,1,'Lima'),(2,1,3,'1996'),(3,1,5,'1'),(4,1,2,'Si'),(5,1,9,'No'),(6,1,6,'Mestizo'),(7,1,10,'Catolico'),(8,1,7,'46978462'),(9,1,8,'No'),(10,1,4,'Si'),(11,2,11,'4'),(12,2,12,'No'),(13,2,14,'Lima'),(14,2,15,'Ayacucho'),(15,2,13,'Si'),(16,2,17,'Si'),(17,2,16,'3'),(18,2,19,'No'),(19,2,18,'0'),(20,2,20,'2'),(21,3,1,'Arequipa'),(22,3,3,'1980'),(23,3,5,'2'),(24,3,2,'Si'),(25,3,9,'Si'),(26,3,6,'Blanco'),(27,3,10,'Evangelico'),(28,3,7,'14698735'),(29,3,8,'No'),(30,3,4,'Si'),(31,4,21,'3'),(32,4,22,'6'),(33,4,27,'San Isidro'),(34,4,24,'Ladrillo'),(35,4,28,'Ladrillo'),(36,4,29,'Ladrillo'),(37,4,25,'Si'),(38,4,26,'Si'),(39,4,30,'7'),(40,4,31,'Gas'),(41,5,1,'Lima'),(42,5,3,'1998'),(43,5,5,'2'),(44,5,2,'Si'),(45,5,9,'No'),(46,5,6,'Mestizo'),(47,5,10,'Catolico'),(48,5,7,'74698523'),(49,5,8,'No'),(50,5,4,'No'),(51,7,1,'Trujillo'),(52,7,3,'1990'),(53,7,5,'3'),(54,7,2,'No'),(55,7,9,'Si'),(56,7,6,'Mestizo'),(57,7,10,'Agnositco'),(58,7,7,'16497818'),(59,7,8,'No'),(60,7,4,'Si'),(61,8,1,'Cusco'),(62,8,3,'1985'),(63,8,5,'2'),(64,8,2,'Si'),(65,8,9,'No'),(66,8,6,'Indigena'),(67,8,10,'Catolico'),(68,8,7,'14697523'),(69,8,8,'Si'),(70,8,4,'Si'),(71,9,21,'2'),(72,9,22,'8'),(73,9,27,'Chorrillos'),(74,9,24,'Ladrillo'),(75,9,28,'Ladrillo'),(76,9,29,'Madera'),(77,9,25,'Si'),(78,9,26,'Si'),(79,9,30,'4'),(80,9,31,'Gas natural'),(81,10,11,'2'),(82,10,12,'Si'),(83,10,14,'Cusco'),(84,10,15,'Cusco'),(85,10,13,'No'),(86,10,17,'Si'),(87,10,16,'4'),(88,10,19,'Si'),(89,10,18,'1'),(90,10,20,'2'),(91,11,21,'1'),(92,11,22,'20'),(93,11,27,'Villa El Salvador'),(94,11,24,'Ladrillo'),(95,11,28,'Cemento'),(96,11,29,'Cemento'),(97,11,25,'Si'),(98,11,26,'Si'),(99,11,30,'5'),(100,11,31,'Gas');
/*!40000 ALTER TABLE `respuesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipopregunta`
--

DROP TABLE IF EXISTS `tipopregunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipopregunta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipopregunta`
--

LOCK TABLES `tipopregunta` WRITE;
/*!40000 ALTER TABLE `tipopregunta` DISABLE KEYS */;
INSERT INTO `tipopregunta` VALUES (1,'Texto'),(2,'Numerico'),(3,'Si/No');
/*!40000 ALTER TABLE `tipopregunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipousuario`
--

DROP TABLE IF EXISTS `tipousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipousuario` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `Descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipousuario`
--

LOCK TABLES `tipousuario` WRITE;
/*!40000 ALTER TABLE `tipousuario` DISABLE KEYS */;
INSERT INTO `tipousuario` VALUES (1,'Encuestado','Completa las encuestas'),(2,'Administrador','Tiene acceso total'),(3,'Director','Crea las encuestas');
/*!40000 ALTER TABLE `tipousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `TipoUsuarioID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Usuario_TipoUsuario` (`TipoUsuarioID`),
  CONSTRAINT `FK_Usuario_TipoUsuario` FOREIGN KEY (`TipoUsuarioID`) REFERENCES `tipousuario` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1010 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Juan Perez','jperez','123','jperez@hotmail.com',2),(2,'Marco Paredes','marcop','123','marcop@hotmail.com',3),(3,'Alonso Diaz','alonsod','123','alonsod@hotmail.com',1),(1002,'Carlos Salazar','carloss','123','carloss@hotmail.com',1),(1004,'Gustavo Pinto','gustavop','123','gustavop@hotmail.com',1),(1008,'Sofia Meza','sofiam','123','sofiam@hotmail.com',1),(1009,'Angela Barreto','angelab','123','angelab@hotmail.com',1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_encuesta`
--

DROP TABLE IF EXISTS `usuario_encuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_encuesta` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UsuarioID` int(11) NOT NULL,
  `EncuestaID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Usuario_Encuesta_Encuesta` (`EncuestaID`),
  KEY `FK_Usuario_Encuesta_Usuario` (`UsuarioID`),
  CONSTRAINT `FK_Usuario_Encuesta_Encuesta` FOREIGN KEY (`EncuestaID`) REFERENCES `encuesta` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Usuario_Encuesta_Usuario` FOREIGN KEY (`UsuarioID`) REFERENCES `usuario` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_encuesta`
--

LOCK TABLES `usuario_encuesta` WRITE;
/*!40000 ALTER TABLE `usuario_encuesta` DISABLE KEYS */;
INSERT INTO `usuario_encuesta` VALUES (1,3,1),(2,3,2),(3,1002,1),(4,1002,3),(5,1004,1),(6,1004,3),(7,1008,1),(8,1009,1),(9,1009,3),(10,1009,2),(11,3,3);
/*!40000 ALTER TABLE `usuario_encuesta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-10 17:47:28
