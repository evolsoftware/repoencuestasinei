<%@page import="model.Usuario"%>
<%@page import="service.implement.UsuarioService"%>
<%@page import="service.IUsuarioService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Perfil</title>
</head>
<body>
<%
	int id_director = Integer.valueOf(request.getParameter("id_director"));
	IUsuarioService usuarioService = new UsuarioService();
	Usuario objUsuario = usuarioService.UsuarioPorId(id_director);
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                 <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
        <h1>Editar perfil</h1>
        <form action="ServletPerfilDirector" method="POST">
	        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
	        <input type="hidden" id="id_tipo_usuario" name="id_tipo_usuario" value="<%=objUsuario.getTipoUsuarioID()%>"/>
	        <table>
	            <tr>
	    			<td>Nombre</td>
	    			<td>
	    			<input type="text" id="txtNombre" name="txtNombre" value="<%=objUsuario.getNombre()%>"/>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>Username</td>
	    			<td>
	    			<input type="text" id="txtUsername" name="txtUsername" value="<%=objUsuario.getUsername()%>"/>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>Contrase�a</td>
	    			<td>
	    			<input type="password" id="txtContrase�a" name="txtContrase�a"/>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>Repetir contrase�a</td>
	    			<td>
	    			<input type="password" id="txtRepetirContrase�a" name="txtRepetirContrase�a"/>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>Correo</td>
	    			<td>
	    			<input type="text" id="txtCorreo" name="txtCorreo" value="<%=objUsuario.getEmail()%>"/>
	    			</td>
	    		</tr>
	    		<% if(request.getAttribute("validesContrase�a") != null){%>
					<tr>
	            		 <td colspan="2" style="color:#FF0016">Contrase�as no adecuadas</td>
					</tr>
			        <%}%>
	    		<tr>
	    			<td>
	    			<input type="submit" id="btnGuardar" name="btnGuardar" value="Guardar"/>
	    			</td>
	    		</tr>
	        </table>
	        
	        <a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Cancelar</a>
        </form>
    </section> 
</body>
</html>