<%@page import="model.Encuesta"%>
<%@page import="java.util.List"%>
<%@page import="service.implement.EncuestaService"%>
<%@page import="service.IEncuestaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Mis encuestas</title>
</head>
<body>
	<% 
		IEncuestaService encuestaService = new EncuestaService();
		List<Encuesta> encuestas = encuestaService.lista();
		int id_director = Integer.valueOf(request.getParameter("id_director"));
	%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
    	<h1>Mis encuestas</h1>
        <a href="director_crear_encuesta.jsp?id_director=<%=id_director%>">Crear encuesta</a>
        <table>
        	<tr>
        		<th>Nombre</th>
        		<th>Costo</th>
        		<th>Alcance nacional</th>
        		<th>Estado</th>
        		<th>Detalles</th>
        		<th>Editar</th>
        		<th>Eliminar</th>
        	</tr>
        	<% for(int a=0; a<encuestas.size();a++) {%>
        	<tr>
        		<td><%=encuestas.get(a).getNombre()%></td>
        		
        		
        		<td><%=encuestas.get(a).getCosto()%></td>
        		<%if(encuestas.get(a).getAlcanceNacional()){%>
        		<td>Si</td>
        		<%}else{ %>
        		<td>No</td>
        		<%} %>
        		<%if(encuestas.get(a).getEstado()){%>
        		<td>Activo</td>
        		<%}else{ %>
        		<td>Inactivo</td>
        		<%} %>
        		<td>
        			<a href="director_detalles_encuesta.jsp?id_director=<%=id_director%>&id_encuesta=<%=encuestas.get(a).getId()%>">Detalles</a>
        		</td>
        		<td>
        			<a href="director_editar_encuesta.jsp?id_director=<%=id_director%>&id_encuesta=<%=encuestas.get(a).getId()%>">Editar</a>
        		</td>
        		<td>
        			<a href="director_eliminar_encuesta.jsp?id_director=<%=id_director%>&id_encuesta=<%=encuestas.get(a).getId()%>">Eliminar</a>
        		</td>
        	</tr>
        	<%} %>
        </table>
    </section> 
</body>
</html>