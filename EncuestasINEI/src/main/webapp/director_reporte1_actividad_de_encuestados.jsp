<%@page import="model.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="service.IUsuarioService"%>
<%@page import="service.implement.UsuarioService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Actividades de encuestados</title>
</head>
<body>
<%
	int id_director = Integer.valueOf(request.getParameter("id_director"));
	IUsuarioService usuarioService = new UsuarioService();
	List<Usuario> usuarios = usuarioService.UsuariosYTotalEncuestasRespondidas();
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
        <h1>Actividades de encuestados</h1>
        
        <table>
        <tr>
        	<th>Nombre</th>
        	<th>Username</th>
        	<th>Email</th>
        	<th>Total encuestas respondidas</th>
        </tr>
        <%for(int a=0;a<usuarios.size();a++){ %>
        <tr>
        	<td><%=usuarios.get(a).getNombre() %></td>
        	<td><%=usuarios.get(a).getUsername() %></td>
        	<td><%=usuarios.get(a).getEmail() %></td>
        	<td align="center"><%=usuarios.get(a).getEncuestasRespondidas() %></td>
        </tr>
        <%} %>
        </table>
    </section> 
</body>
</html>