<%@page import="model.TipoUsuario"%>
<%@page import="service.implement.TipoUsuarioService"%>
<%@page import="service.ITipoUsuarioService"%>
<%@page import="model.TipoPregunta"%>
<%@page import="service.implement.TipoPreguntaService"%>
<%@page import="service.ITipoPreguntaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Eliminar tipo de usuario</title>
</head>
<body>
<form action="ServletEliminarTipoUsuario" method="POST">
<%
	int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
	int id_tipo_usuario = Integer.valueOf(request.getParameter("id_tipo_usuario"));
	ITipoUsuarioService tipoUsuarioService = new TipoUsuarioService();
	TipoUsuario objTipoUsuario = tipoUsuarioService.TipoUsuarioPorID(id_tipo_usuario);
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
    <input type="hidden" id="id_administrador" name="id_administrador" value="<%=id_administrador%>"/>
    <input type="hidden" id="id_tipo_usuario" name="id_tipo_usuario" value="<%=id_tipo_usuario%>"/>
    	<h1>�Est� seguro de eliminar este usuario?</h1>
    	<table>
    		<tr>
    			<td>Nombre</td>
    			<td>:<%=objTipoUsuario.getNombre()%></td>
    		</tr>
    		<tr>
    			<td>Descripcion</td>
    			<td>:<%=objTipoUsuario.getDescripcion()%></td>
    		</tr>
    		<tr>
        		<td>
        			<input type="submit" id="btnEliminar" name="bntEliminar" value="Eliminar"/>
        		</td>
        	</tr>
    	</table>
    	<a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Volver</a>
    </section>
</form>
</body>
</html>