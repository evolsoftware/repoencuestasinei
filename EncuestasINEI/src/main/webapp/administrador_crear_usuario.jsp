<%@page import="service.implement.TipoUsuarioService"%>
<%@page import="service.ITipoUsuarioService"%>
<%@page import="service.implement.UsuarioService"%>
<%@page import="service.IUsuarioService"%>
<%@page import="model.TipoUsuario"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Crear usuario</title>
</head>
<body>

<body>
<form action="ServletCrearUsuario" method="POST">
<%
	ITipoUsuarioService ItipoUsuarioService = new TipoUsuarioService();
	List<TipoUsuario> tipoUsuarios = ItipoUsuarioService.lista();
	int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
%>
	<header>
       <section class="wrapper">
             <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
    <input type="hidden" id="id_administrador" name="id_administrador" value="<%=id_administrador%>"/>
    	<h1>Crear usuario</h1>
    	<table>
    		<tr>
    			<td>Nombre</td>
    			<td>
    			<input type="text" id="txtNombre" name="txtNombre"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Username</td>
    			<td>
    			<input type="text" id="txtUsername" name="txtUsername"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Password</td>
    			<td>
    			<input type="password" id="txtPassword" name="txtPassword"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Email</td>
    			<td>
    			<input type="text" id="txtEmail" name="txtEmail"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Tipo de usuario</td>
    			<td>
    				<select name="sTipoUsuario">
    					<%for(int i = 0;i<tipoUsuarios.size();i++){ %>
    						<option value="<%=tipoUsuarios.get(i).getId()%>"><%=tipoUsuarios.get(i).getNombre() %></option>
    					<%} %>
    				</select>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<input type="submit" id="btnCrear" name="btnCrear" value="Crear"/>
    			</td>
    		</tr>
    	</table>
    	<a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Volver</a>
	</section>
</form>
</body>
</html>