<%@page import="model.Usuario"%>
<%@page import="service.implement.UsuarioService"%>
<%@page import="java.util.List"%>
<%@page import="service.IUsuarioService"%>
<%@page import="model.Encuesta"%>
<%@page import="service.implement.EncuestaService"%>
<%@page import="service.IEncuestaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Detalle del usuario</title>
</head>
<body>
<%
	IUsuarioService usuarioService = new UsuarioService();
	List<Usuario> usuarios = usuarioService.lista();
	int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
	int id_usuario = Integer.valueOf(request.getParameter("id_usuario"));
	Usuario usuario = usuarioService.UsuarioPorId(id_usuario);
	
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_administrador" name="id_administrador" value="<%=id_administrador%>"/>
        <h1>Detalles del usuario</h1>
        <table>
        	<tr>
        		<td>Nombre</td>
        		<td>:<%=usuario.getNombre()%></td>
        	</tr>
        	<tr>
        		<td>Username</td>
        		<td>:<%=usuario.getUsername()%></td>
        	</tr>
        	<tr>
        		<td>Password</td>
        		<td>:<%=usuario.getPassword()%></td>
        	</tr>
        	<tr>
        		<td>Email</td>
        		<td>:<%=usuario.getEmail()%></td>
        	</tr>
        	<tr>
        		<td>Tipo de usuario</td>
        		<td>:<%=usuario.getTipoUsuarioID()%></td>
        	</tr>
        </table>
        
        <a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Volver</a>
    </section> 
</body>
</html>