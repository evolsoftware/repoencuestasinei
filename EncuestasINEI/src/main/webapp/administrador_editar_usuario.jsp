<%@page import="service.implement.TipoUsuarioService"%>
<%@page import="service.ITipoUsuarioService"%>
<%@page import="model.TipoUsuario"%>
<%@page import="service.implement.UsuarioService"%>
<%@page import="service.IUsuarioService"%>
<%@page import="java.util.List"%>
<%@page import="model.Usuario"%>
<%@page import="model.Encuesta"%>
<%@page import="service.implement.EncuestaService"%>
<%@page import="service.IEncuestaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Insert title here</title>
</head>
<body>
<form action="ServletEditarUsuario" method="POST">
<%
	IUsuarioService usuarioService = new UsuarioService();
	ITipoUsuarioService tipoUsuarioService = new TipoUsuarioService();
	int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
	int id_usuario = Integer.valueOf(request.getParameter("id_usuario"));
	Usuario usuario = usuarioService.UsuarioPorId(id_usuario);
	TipoUsuario tipoUsuario = tipoUsuarioService.TipoUsuarioPorID(usuario.getTipoUsuarioID());
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_administrador" name="id_administrador" value="<%=id_administrador%>"/>
        <input type="hidden" id="id_usuario" name="id_usuario" value="<%=id_usuario%>"/>
        <h1>Editar usuario</h1>
        <table>
    		<tr>
    			<td>Nombre</td>
    			<td>
    			<input type="text" id="txtNombre" name="txtNombre" value="<%=usuario.getNombre()%>"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Username</td>
    			<td>
    			<input type="text" id="txtUsername" name="txtUsername" value="<%=usuario.getUsername()%>"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Password</td>
    			<td>
    			<input type="password" id="txtPassword" name="txtPassword" value="<%=usuario.getPassword()%>"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Email</td>
    			<td>
    			<input type="text" id="txtEmail" name="txtEmail" value="<%=usuario.getEmail()%>"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Tipo de usuario</td>
    			<td>
    				<select name="sTipoUsuario">
    					<option value="<%=usuario.getTipoUsuarioID()%>"><%=tipoUsuario.getNombre() %></option>
    				</select>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<input type="submit" id="btnGuardar" name="btnGuardar" value="Guardar"/>
    			</td>
    		</tr>
        </table> 
    	<a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Volver</a>    
    </section> 
</form>
</body>
</html>