<%@ page import="model.Encuesta" %>
<%@ page import="model.Pregunta" %>
<%@ page import="service.IEncuestaService" %>
<%@ page import="service.implement.EncuestaService" %>
<%@ page import="service.IPreguntaService" %>
<%@ page import="service.implement.PreguntaService" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Responder Encuesta</title>
</head>
<body>
 		<%
        int id_encuestado=Integer.valueOf(request.getParameter("id_encuestado"));
 		int id_encuesta=Integer.valueOf(request.getParameter("id_encuesta"));
    	IEncuestaService es=new EncuestaService();
    	IPreguntaService ps=new PreguntaService();
    	Encuesta encuesta=es.EncuestaPorID(id_encuesta);
    	List<Pregunta> preguntas=ps.PreguntasEncuesta(id_encuesta);
    	%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="encuestado_encuestas_nuevas.jsp?id_encuestado=<%=id_encuestado%>">Encuestas nuevas</a></li>
                <li><a href="encuestado_mis_encuestas.jsp?id_encuestado=<%=id_encuestado%>">Mis encuestas</a></li>
                <li><a href="encuestado_perfil.jsp?id_encuestado=<%=id_encuestado%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
    	
    	<h3>Encuesta: <%=encuesta.getNombre() %></h3>
    	<p><%=encuesta.getDescripcion() %></p>
    	<form action="ServletResponderEncuesta" method="post">
    		<input type="hidden" id="id_encuestado" name="id_encuestado" value="<%=id_encuestado%>"/>
    		<input type="hidden" id="id_encuesta" name="id_encuesta" value="<%=id_encuesta%>"/>
    		<table>
    			<tr>    			
    				<th>Pregunta</th>
    				<th>Respuesta</th>
    				<th></th>
    			</tr>
    			<%for(int i=0;i<preguntas.size();i++){%>
    			<tr>
    				<td><%=preguntas.get(i).getDescripcion()%></td>
    				<td><%
    					switch(preguntas.get(i).getTipoPreguntaID()){
    					case 2:{%>
    						<input type="number" id="txtRespuesta<%=i%>" name="txtRespuesta<%=i%>">
    					<%}break;
    					case 3:{%>
    						<select id="txtRespuesta<%=i%>" name="txtRespuesta<%=i%>">
    							<option value="Si">Si</option>
    							<option value="No">No</option>
    						</select>
    					<%}break;
    					default:{%>
    						<input type="text" id="txtRespuesta<%=i%>" name="txtRespuesta<%=i%>">
    					<%};
    					}
    				%></td>
    				<td>
    					<input type="hidden" id="idp<%=preguntas.get(i).getId()%>" name="idp<%=preguntas.get(i).getId()%>" value="<%=preguntas.get(i).getId()%>">
    				</td>
    			</tr>
    			<%}%>
    			<tr>
    				<td></td>
    				<td><a href="encuestado_encuestas_nuevas.jsp?id_encuestado=<%=id_encuestado%>">Cancelar</a></td>
    				<td><input type="submit" id="btnGuardar" name="btnGuardar" value="Guardar"/></td>
    			</tr>
        	</table>
        	
    	</form>
    </section> 
</body>
</html>