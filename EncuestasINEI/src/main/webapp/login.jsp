<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Iniciar Sesi�n</title>
</head>
<body>
<form action="ServletLogin" method="POST">
	<h1>Iniciar Sesi�n</h1>
	<table>
		<tr>
			<td>Username</td>
			<td>
				<input type="text" id="txtUsername" name="txtUsername"/>
			</td>
		</tr>
		<tr>
			<td>Contrase�a</td>
			<td>
				<input type="password" id="txtContrase�a" name="txtContrase�a"/>
			</td>
		</tr>
		<% if(request.getAttribute("usuarioValido") != null){%>
           <tr>
             <td colspan="2" style="color:#FF0016">Usuario o contrase�a incorrecta</td>
           </tr>
        <%}%>
		<tr>
			<td>
				<input type="submit" id="btnIniciarSesion" name="btnIniciarSesion" value="Iniciar Sesi�n"/>
			</td>
		</tr>
	</table>
</form>
</body>
</html>