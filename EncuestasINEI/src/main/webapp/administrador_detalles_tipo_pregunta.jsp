<%@page import="model.TipoPregunta"%>
<%@page import="service.implement.TipoPreguntaService"%>
<%@page import="service.ITipoPreguntaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Detalle de tipo de pregunta</title>
</head>
<body>
<%
	int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
	int id_tipo_pregunta = Integer.valueOf(request.getParameter("id_tipo_pregunta"));
	ITipoPreguntaService tipoPreguntaService = new TipoPreguntaService();
	TipoPregunta objTipoPregunta = tipoPreguntaService.TipoPreguntaPorID(id_tipo_pregunta);
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
    	<h1>Detalles del tipo de pregunta</h1>
    	<table>
    		<tr>
    			<td>Nombre</td>
    			<td>:<%=objTipoPregunta.getNombre()%></td>
    		</tr>
    	</table>
    	<a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Volver</a>
    </section>
</body>
</html>