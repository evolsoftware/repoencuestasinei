<%@page import="java.util.Arrays"%>
<%@page import="model.Encuesta"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Encuestas por fecha</title>
</head>
<body>
	<form action="ServletBuscarEncuestaPorFecha" method="POST">
<%
	int id_director = Integer.valueOf(request.getParameter("id_director"));
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">

        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
        <h1>Encuestas por fecha</h1>
        <table>
        	<tr>
        		<td>Desde</td>
        		<td>
        			<input type="date" id="txtDesde" name="txtDesde"/>
        		</td>
        	</tr>
        	<tr>
        		<td>Hasta</td>
        		<td>
        			<input type="date" id="txtHasta" name="txtHasta"/>
        		</td>
        	</tr>
        	<tr>
        		<td>Activos</td>
        		<td>
        			<input type="checkbox" id="cbActivos" name="cbActivos"/>
        		</td>
        	</tr>
        	<tr>
        		<td>Inactivos</td>
        		<td>
        			<input type="checkbox" id="cbInactivos" name="cbInactivos"/>
        		</td>
        	</tr>
        	<tr>
        		<td colspan="1">
        			<input type="submit" id="btnBuscar" name="btnBuscar" value="Buscar"/>
        		</td>
        	</tr>
        </table>
   </form> 
        <table>
        	<tr>
        		<th>Nombre</th>
        		<th>Descripcion</th>
        		<th>Costo</th>
        		<th>Alcance nacional</th>
        	</tr>
        	<% 
        		HttpSession objSession = request.getSession(false);
        		List<Encuesta> encuestas = new ArrayList<Encuesta>();
        		if(request.getAttribute("encuestas") != null){
        			String s="";
        			encuestas = (ArrayList<Encuesta>)request.getAttribute("encuestas");
        		}
        	 %>
        	<% for(int a=0; a<encuestas.size();a++) {%>
        	<tr>
        		<td><%=encuestas.get(a).getNombre()%></td>  
        		<td><%=encuestas.get(a).getDescripcion()%></td>       		       		
        		<td><%=encuestas.get(a).getCosto()%></td>
        		<%if(encuestas.get(a).getAlcanceNacional()){%>
        		<td>Si</td>
        		<%}else{ %>
        		<td>No</td>
        		<%} %>
        	</tr>
        	<%} %>
        </table>
   
    </section> 
</body>
</html>