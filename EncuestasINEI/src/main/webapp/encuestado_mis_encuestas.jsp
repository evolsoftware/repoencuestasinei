<%@ page import="model.Encuesta" %>
<%@ page import="service.IEncuestaService" %>
<%@ page import="service.implement.EncuestaService" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Mis encuestas</title>
</head>
<body>
 		<%
        int id_encuestado=Integer.valueOf(request.getParameter("id_encuestado"));
    	IEncuestaService es=new EncuestaService();
    	List<Encuesta> encuestas=(List<Encuesta> )es.EncuestasRespondidasDeUsuarios(id_encuestado);
    	%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="encuestado_encuestas_nuevas.jsp?id_encuestado=<%=id_encuestado%>">Encuestas nuevas</a></li>
                <li><a href="encuestado_mis_encuestas.jsp?id_encuestado=<%=id_encuestado%>">Mis encuestas</a></li>
                <li><a href="encuestado_perfil.jsp?id_encuestado=<%=id_encuestado%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
    	<table>
    		<thead>
    			<tr>
    				<th>Nombre</th>
    				<th>Descripci�n</th>
    				<th>Fecha</th>
    				<th>Opci�n</th>
    			</tr>
    		</thead>
    		<tbody>
    			<%for(int i=0;i<(int)encuestas.size();i++){ %>
    			<tr>
    				<td><%=encuestas.get(i).getNombre()%></td>
    				<td><%=encuestas.get(i).getDescripcion()%></td>
    				<td><%=encuestas.get(i).getFecha()%></td>
    				<td><a href="encuestadoVerEncuesta.jsp?id_encuestado=<%=id_encuestado%>&id_encuesta=<%=encuestas.get(i).getId()%>">Ver</a></td>
    			</tr>
    			<%}%>
    		</tbody>
    	</table>
    </section> 
</body>
</html>