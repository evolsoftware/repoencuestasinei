<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Reportes</title>
</head>
<body>
<%
	int id_director = Integer.valueOf(request.getParameter("id_director"));
%>
	<header>
       <section class="wrapper">
              <nav >
                <ul class="nav">
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a> </li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
        <table>
      	  <tr>
      	  	<td><a href="director_reporte1_actividad_de_encuestados.jsp?id_director=<%=id_director%>">Actividad de encuestados</a></td>
      	  </tr>
      	  <tr>
      	  	<td><a href="director_reporte2_usuarios_por_encuesta.jsp?id_director=<%=id_director%>">Usuarios por encuesta</a></td>
      	  </tr>
      	  <tr>
      	  	<td><a href="director_reporte3_encuestas_por_fecha.jsp?id_director=<%=id_director%>">Encuestas por fecha</a></td>
      	  </tr>
      	  <tr>
      	  	<td><a href="director_reporte4_encuestas_por_costo.jsp?id_director=<%=id_director%>">Encuestas por costo</a></td>
      	  </tr>
      	  <tr>
      	  	<td><a href="director_reporte5_top_encuestas.jsp?id_director=<%=id_director%>">Top encuestas</a></td>
      	  </tr>
        </table>     
    </section> 
</body>
</html>