<%@page import="model.Encuesta"%>
<%@page import="service.implement.EncuestaService"%>
<%@page import="service.IEncuestaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Insert title here</title>
</head>
<body>
<form action="ServletEditarEncuesta" method="POST">
<%
	int id_director = Integer.valueOf(request.getParameter("id_director"));
	int id_encuesta = Integer.valueOf(request.getParameter("id_encuesta"));
	IEncuestaService encuestaService = new EncuestaService();
	Encuesta objEncuesta = encuestaService.EncuestaPorID(id_encuesta);
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
        <input type="hidden" id="id_encuesta" name="id_encuesta" value="<%=id_encuesta%>"/>
        <h1>Editar encuesta</h1>
        <table>
    		<tr>
    			<td>Nombre</td>
    			<td>
    			<input type="text" id="txtNombre" name="txtNombre" value="<%=objEncuesta.getNombre()%>"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Descripci�n</td>
    			<td>
    			<input type="text" id="txtDescripcion" name="txtDescripcion" value="<%=objEncuesta.getDescripcion()%>"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Fecha</td>
    			<td>
    			<input type="date" id="txtFecha" name="txtFecha" value="<%=objEncuesta.getFecha()%>"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Costo</td>
    			<td>
    			<input type="number" id="txtCosto" name="txtCosto" value="<%=objEncuesta.getCosto()%>"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Alcance nacional</td>
    			<td>
    			<input type="checkbox" id="cbAlcanceNacional" name="cbAlcanceNacional"
    			  <%if(objEncuesta.getAlcanceNacional()){%>checked="checked"<%} %>/>
    			</td>
    		</tr>
    		<tr>
    			<td>Estado</td>
    			<td>
    			<input type="checkbox" id="cbEstado" name="cbEstado"  
    				<%if(objEncuesta.getEstado()){%>checked="checked"<%} %>/>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<input type="submit" id="btnGuardar" name="btnGuardar" value="Guardar"/>
    			</td>
    		</tr>
        </table> 
    	<a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Volver</a>    
    </section> 
</form>
</body>
</html>