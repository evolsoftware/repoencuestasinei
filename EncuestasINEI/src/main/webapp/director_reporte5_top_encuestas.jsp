<%@page import="model.Encuesta"%>
<%@page import="java.util.List"%>
<%@page import="service.implement.EncuestaService"%>
<%@page import="service.IEncuestaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Top encuestas</title>
</head>
<body>
<%
	int id_director = Integer.valueOf(request.getParameter("id_director"));
	IEncuestaService encuestaService = new EncuestaService();
	List<Encuesta> encuestas = encuestaService.TopEncuestas();	
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
        <h1>Top encuestas resueltas</h1>
        <table>
        <tr>
        	<th>Nombre</th>
        	<th>Descripcion</th>
        	<th>Costo</th>
        	<th>Alcance Nacional</th>
        	<th>Total resueltas</th>
        </tr>
        <%for(int a=0;a<encuestas.size();a++){ %>
        <tr>
        	<td><%=encuestas.get(a).getNombre() %></td>
        	<td><%=encuestas.get(a).getDescripcion() %></td>
        	<td><%=encuestas.get(a).getCosto() %></td>
        	<%if(encuestas.get(a).getAlcanceNacional()){%>
        	<td>Si</td>
        	<%}else{ %>
        	<td>No</td>
        	<%} %>
        	<td align="center"><%=encuestas.get(a).getTotalResueltos() %></td>
        </tr>
        <%} %>
        </table>   
    </section> 
</body>
</html>