<%@page import="model.Encuesta"%>
<%@page import="service.implement.EncuestaService"%>
<%@page import="service.IEncuestaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Detalle de encuesta</title>
</head>
<body>
<%
	int id_director = Integer.valueOf(request.getParameter("id_director"));
	int id_encuesta = Integer.valueOf(request.getParameter("id_encuesta"));
	IEncuestaService encuestaService = new EncuestaService();
	Encuesta objEncuesta = encuestaService.EncuestaPorID(id_encuesta);
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
        <h1>Detalles de la encuesta</h1>
        <table>
        	<tr>
        		<td>Nombre</td>
        		<td>:<%=objEncuesta.getNombre()%></td>
        	</tr>
        	<tr>
        		<td>Descripci�n</td>
        		<td>:<%=objEncuesta.getDescripcion()%></td>
        	</tr>
        	<tr>
        		<td>Fecha</td>
        		<td>:<%=objEncuesta.getFecha()%></td>
        	</tr>
        	<tr>
        		<td>Costo</td>
        		<td>:<%=objEncuesta.getCosto()%></td>
        	</tr>
        	<tr>
        		<td>Alcance nacional</td>
        		<%if(objEncuesta.getAlcanceNacional()){%>
        		<td>:Si</td>
        		<%}else{ %>
        		<td>:No</td>
        		<%} %>
        	</tr>
        	<tr>
        		<td>Estado</td>
        		<%if(objEncuesta.getEstado()){%>
        		<td>:Activo</td>
        		<%}else{ %>
        		<td>:Inactivo</td>
        		<%} %>
        	</tr>
        </table>
        
        <a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Volver</a>
    </section> 
</body>
</html>