<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Crear tipo de pregunta</title>
</head>
<body>
<form action="ServletCrearTipoUsuario" method="POST">
<%
	int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
    <input type="hidden" id="id_administrador" name="id_administrador" value="<%=id_administrador%>"/>
    	<h1>Crear tipo de pregunta</h1>
    	<table>
    		<tr>
    			<td>Nombre</td>
    			<td>
    				<input type="text" id="txtNombre" name="txtNombre"/>
    			</td>
    		</tr>
    		<tr>
    			<td>Descripcion</td>
    			<td>
    				<input type="text" id="txtDescripcion" name="txtDescripcion"/>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<input type="submit" id="btnCrear" name="btnCrear" value="Crear"/>
    			</td>
    		</tr>
    	</table>
    	<a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Volver</a>
    </section>
</form>
</body>
</html>