
<%@page import="model.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="service.implement.UsuarioService"%>
<%@page import="service.IUsuarioService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Usuarios</title>
</head>
<body>
	<% 
		IUsuarioService usuarioService = new UsuarioService();
		List<Usuario> usuarios = usuarioService.lista();
		int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
	%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <h1>Usuarios</h1>
        <a href="administrador_crear_usuario.jsp?id_administrador=<%=id_administrador%>">Crear usuario</a>
        <table>
        	<tr>
        		<th>Username</th>
        		<th>Contrase�a</th>
        		<th>Tipo de usuario</th>
        		<th>Detalles</th>
        		<th>Editar</th>
        		<th>Eliminar</th>
        	</tr>
        	<% for(int a=0; a<usuarios.size();a++) {%>
        	<tr>
        		<td><%=usuarios.get(a).getUsername()%></td>
        		<td><%=usuarios.get(a).getPassword()%></td>
        		<%if(usuarios.get(a).getTipoUsuarioID() == 1){%>
        		<td>Encuestado</td>
        		<%}else if(usuarios.get(a).getTipoUsuarioID() == 2){ %>
        		<td>Administrador</td>
        		<%} else { %>
        		<td>Director</td>
        		<%} %>
        		<td>
        			<a href="administrador_detalles_usuario.jsp?id_administrador=<%=id_administrador%>&id_usuario=<%=usuarios.get(a).getId()%>">Detalles</a>
        		</td>
        		<td>
        			<a href="administrador_editar_usuario.jsp?id_administrador=<%=id_administrador%>&id_usuario=<%=usuarios.get(a).getId()%>">Editar</a>
        		</td>
        		<td>
        			<a href="administrador_eliminar_usuario.jsp?id_administrador=<%=id_administrador%>&id_usuario=<%=usuarios.get(a).getId()%>">Eliminar</a>
        		</td>
        	</tr>
        	<%} %>
        </table>
    </section> 
</body>
</html>