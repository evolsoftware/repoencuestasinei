<%@page import="model.TipoPregunta"%>
<%@page import="java.util.List"%>
<%@page import="service.implement.TipoPreguntaService"%>
<%@page import="service.ITipoPreguntaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Tipos de preguntas</title>
</head>
<body>
	<%
		ITipoPreguntaService tipoPreguntaService = new TipoPreguntaService();
		List<TipoPregunta> tiposPreguntas = tipoPreguntaService.lista();
		int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
	%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <h1>Tipo de preguntas</h1>
        <a href="administrador_crear_tipo_pregunta.jsp?id_administrador=<%=id_administrador%>">Crear tipo de pregunta</a>
        <table>
        	<tr>
        		<th>Nombre</th>
        		<th>Detalles</th>
        		<th>Editar</th>
        		<th>Eliminar</th>
        	</tr>
        	<%for(int i = 0; i < tiposPreguntas.size(); i++ ){%>
        	<tr>
        		<td><%=tiposPreguntas.get(i).getNombre()%></td>
        		<td>
        			<a href="administrador_detalles_tipo_pregunta.jsp?id_administrador=<%=id_administrador%>&id_tipo_pregunta=<%=tiposPreguntas.get(i).getId()%>">Detalles</a>
        		</td>
        		<td>
        			<a href="administrador_editar_tipo_pregunta.jsp?id_administrador=<%=id_administrador%>&id_tipo_pregunta=<%=tiposPreguntas.get(i).getId()%>">Editar</a>
        		</td>
        		<td>
        			<a href="administrador_eliminar_tipo_pregunta.jsp?id_administrador=<%=id_administrador%>&id_tipo_pregunta=<%=tiposPreguntas.get(i).getId()%>">Eliminar</a>
        		</td>
        	</tr>
        	<%}%>        
        </table>
    </section> 
</body>
</html>