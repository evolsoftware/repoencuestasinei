<%@page import="service.implement.TipoUsuarioService"%>
<%@page import="service.implement.UsuarioService"%>
<%@page import="model.TipoUsuario"%>
<%@page import="model.Usuario"%>
<%@page import="service.ITipoUsuarioService"%>
<%@page import="service.IUsuarioService"%>
<%@page import="model.Encuesta"%>
<%@page import="service.implement.EncuestaService"%>
<%@page import="service.IEncuestaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Eliminar encuesta</title>
</head>
<body>
<form action="ServletEliminarUsuario" method="POST">
<%
	IUsuarioService usuarioService = new UsuarioService();
	ITipoUsuarioService tipoUsuarioService = new TipoUsuarioService();
	int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
	int id_usuario = Integer.valueOf(request.getParameter("id_usuario"));
	Usuario usuario = usuarioService.UsuarioPorId(id_usuario);
	TipoUsuario tipoUsuario = tipoUsuarioService.TipoUsuarioPorID(usuario.getTipoUsuarioID());
%>
	<header>
       <section class="wrapper">
               <nav>
                <ul>
                <li><a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Usuarios</a></li>
                <li><a href="administrador_tipos_usuarios.jsp?id_administrador=<%=id_administrador%>">Tipos de usuarios</a></li>
                <li><a href="administrador_tipos_preguntas.jsp?id_administrador=<%=id_administrador%>">Tipo de preguntas</a></li>
                <li><a href="administrador_perfil.jsp?id_administrador=<%=id_administrador%>">Mi perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_administrador" name="id_administrador" value="<%=id_administrador%>"/>
        <input type="hidden" id="id_usuario" name="id_usuario" value="<%=id_usuario%>"/>
        <h1>�Est� seguro de eliminar este usuario?</h1>
        <table>
        	<tr>
    			<td>Nombre</td>
    			<td>:<%=usuario.getNombre()%></td>
    		</tr>
    		<tr>
    			<td>Username</td>
    			<td>:<%=usuario.getUsername()%></td>
    		</tr>
    		<tr>
    			<td>Password</td>
    			<td>:<%=usuario.getPassword()%></td>
    		</tr>
    		<tr>
    			<td>Email</td>
    			<td>:<%=usuario.getEmail()%></td>
    		</tr>
    		<tr>
    			<td>Tipo de usuario</td>
    			<td>:<%=tipoUsuario.getNombre() %></td>
    		</tr>
        	<tr>
        		<td>
        			<input type="submit" id="btnEliminar" name="bntEliminar" value="Eliminar"/>
        		</td>
        	</tr>
        </table>   
        <a href="administrador_usuarios.jsp?id_administrador=<%=id_administrador%>">Volver</a>  
    </section> 
</form>
</body>
</html>