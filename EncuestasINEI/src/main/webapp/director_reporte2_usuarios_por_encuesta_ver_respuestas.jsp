<%@ page import="model.Encuesta" %>
<%@ page import="model.Pregunta" %>
<%@ page import="service.IEncuestaService" %>
<%@ page import="service.implement.EncuestaService" %>
<%@ page import="service.IPreguntaService" %>
<%@ page import="service.implement.PreguntaService" %>
<%@ page import="service.IUsuario_EncuestaService" %>
<%@ page import="service.implement.Usuario_EncuestaService" %>
<%@ page import="service.IRespuestaService" %>
<%@ page import="service.implement.RespuestaService" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Ver Encuesta Respondida</title>
</head>
<body>
<%
		int id_director=Integer.valueOf(request.getParameter("id_director"));
        int id_encuestado=Integer.valueOf(request.getParameter("id_encuestado"));
 		int id_encuesta=Integer.valueOf(request.getParameter("id_encuesta"));
 		
    	IEncuestaService es=new EncuestaService();
    	IPreguntaService ps=new PreguntaService();
    	IRespuestaService rs=new RespuestaService();
    	IUsuario_EncuestaService ues=new Usuario_EncuestaService();
    	
    	Encuesta encuesta=es.EncuestaPorID(id_encuesta);
    	int id_usuario_encuesta=ues.GetIdxUsuarioxEncuesta(id_encuestado, id_encuesta);
    	List<Pregunta> preguntas=ps.PreguntasEncuesta(id_encuesta);
    	
    	%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>           
    </header>
    <section class="contenido wrapper">
  		<h1>Respuestas</h1>  	
    	<h3>Encuesta: <%=encuesta.getNombre() %></h3>
    	<p><%=encuesta.getDescripcion() %></p>
    		<table>
    			<tr>    			
    				<th>Pregunta</th>
    				<th>Respuesta</th>
    			</tr>
    			<%for(int i=0;i<preguntas.size();i++){%>
    			<tr>
    				<td><%=preguntas.get(i).getDescripcion()%></td>
    				<%String respuesta=rs.Texto_respuestaxUExP(id_usuario_encuesta, preguntas.get(i).getId()); %>
    				<td>:<%=respuesta%></td>
    			</tr>
    			<%}%>
        	</table>
        <a href="director_reporte2_usuarios_por_encuesta_ver_usuarios.jsp?id_director=<%=id_director%>&id_encuesta=<%=id_encuesta%>">Volver</a>	
    </section> 
</body>
</html>