<%@page import="model.Encuesta"%>
<%@page import="java.util.List"%>
<%@page import="service.implement.EncuestaService"%>
<%@page import="service.IEncuestaService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/menuEstilo.css"/>
<title>Usuarios por encuesta</title>
</head>
<body>
<%
	int id_director = Integer.valueOf(request.getParameter("id_director"));
	IEncuestaService encuestaService = new EncuestaService();
	List<Encuesta> encuestasActivas = encuestaService.EncuestasParaUsuarios();
%>
	<header>
       <section class="wrapper">
              <nav>
                <ul>
                <li><a href="director_mis_encuestas.jsp?id_director=<%=id_director%>">Mis encuestas</a></li>
                <li><a href="director_reportes.jsp?id_director=<%=id_director%>">Reportes</a></li>
                <li><a href="director_perfil.jsp?id_director=<%=id_director%>">Mi Perfil</a></li>
                <li><a href="login.jsp">Cerrar sesi�n</a></li>
              </ul>
           </nav>
        </section>          
    </header>
    <section class="contenido wrapper">
        <input type="hidden" id="id_director" name="id_director" value="<%=id_director%>"/>
        <h1>Encuestas activas</h1>
        <table>
        	<tr>
        		<th>Nombre</th>
        		<th>Descripci�n</th>
        		<th>Usuarios</th>
        	</tr>
        	<%for(int a=0;a<encuestasActivas.size();a++){ %>
        	<tr>
        		<td><%=encuestasActivas.get(a).getNombre() %></td>
        		<td><%=encuestasActivas.get(a).getDescripcion() %></td>
        		<td><a href="director_reporte2_usuarios_por_encuesta_ver_usuarios.jsp?id_director=<%=id_director%>&id_encuesta=<%=encuestasActivas.get(a).getId()%>">Ver</a></td>
        	</tr>
        	<%} %>
        </table>
    </section> 
</body>
</html>