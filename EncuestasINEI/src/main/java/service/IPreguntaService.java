package service;

import java.util.List;

import model.Pregunta;

public interface IPreguntaService {
	public boolean crear(Pregunta obj);
	public boolean eliminar(int id);
	public boolean actualizar(Pregunta obj);
	public List<Pregunta> lista();
	public List<Pregunta> PreguntasEncuesta(int id_encuesta);
}
