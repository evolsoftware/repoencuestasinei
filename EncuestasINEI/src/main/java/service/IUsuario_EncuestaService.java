package service;

import java.util.List;
import model.Usuario_Encuesta;

public interface IUsuario_EncuestaService {
	public boolean crear(Usuario_Encuesta obj);
	public boolean eliminar(int id);
	public boolean actualizar(Usuario_Encuesta obj);
	public List<Usuario_Encuesta> lista();
	public int GetIdxUsuarioxEncuesta(int id_usuario, int id_encuesta);
}
