package service;

import java.util.List;

import model.Respuesta;

public interface IRespuestaService {
	public boolean crear(Respuesta obj);
	public boolean eliminar(int id);
	public boolean actualizar(Respuesta obj);
	public List<Respuesta> lista();
	public boolean ResolverEncuesta(List<Respuesta> respuestas);
	public List<Respuesta> DetallesEncuestaResuelta(int id_usuario_encuesta);
	public String Texto_respuestaxUExP(int id_usuario_encuesta, int id_pregunta);
}
