package service;

import java.util.List;

import model.Usuario;

public interface IUsuarioService {
	public boolean crear(Usuario obj);
	public boolean eliminar(int id);
	public boolean actualizar(Usuario obj);
	public List<Usuario> lista();
	public List<Usuario> UsuariosPorEncuesta(int id_encuesta);
	public Usuario login(String username, String password);
	public boolean EditarUsuarioLogueado(Usuario obj);
	public Usuario UsuarioPorId(int id_usuario);
	public List<Usuario> UsuariosYTotalEncuestasRespondidas();
}
