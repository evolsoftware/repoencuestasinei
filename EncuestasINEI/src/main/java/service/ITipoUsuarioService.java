package service;

import java.util.List;

import model.TipoUsuario;

public interface ITipoUsuarioService {
	public boolean crear(TipoUsuario obj);
	public boolean eliminar(int id);
	public boolean actualizar(TipoUsuario obj);
	public List<TipoUsuario> lista();
	public TipoUsuario TipoUsuarioPorID(int id);
}
