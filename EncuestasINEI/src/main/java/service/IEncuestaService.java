package service;

import java.sql.Date;
import java.util.List;

import model.Encuesta;

public interface IEncuestaService {
	public boolean crear(Encuesta obj);
	public boolean eliminar(int id);
	public boolean actualizar(Encuesta obj);
	public List<Encuesta> lista(); 
	public List<Encuesta> EncuestasParaUsuarios();
	public List<Encuesta> EncuestasRespondidasDeUsuarios(int id_usuario);
	public List<Encuesta> EncuestasRealizadas();
	public List<Encuesta> EncuestasPorCosto(Double desde, Double hasta, boolean activos, boolean inactivos);
	public List<Encuesta> TopEncuestas();
	public List<Encuesta> EncuestasPorFecha(Date desde, Date hasta, boolean activos, boolean inactivos);
	public List<Encuesta> reporte1();
	public List<Encuesta> reporte5();
	public Encuesta EncuestaPorID(int id);
}
