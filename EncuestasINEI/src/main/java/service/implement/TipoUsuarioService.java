package service.implement;

import java.util.List;

import dao.ITipoUsuarioDAO;
import dao.implement.TipoUsuarioDAO;
import model.TipoUsuario;
import service.ITipoUsuarioService;

public class TipoUsuarioService implements ITipoUsuarioService {

	private ITipoUsuarioDAO tipoUsuarioDAO;
	public TipoUsuarioService() {
		tipoUsuarioDAO = new TipoUsuarioDAO();
	}
	@Override
	public boolean crear(TipoUsuario obj) {
		//ITipoUsuarioDAO tipoUsuarioDAO = new TipoUsuarioDAO();
		return tipoUsuarioDAO.crear(obj);
	}

	@Override
	public boolean eliminar(int id) {
		//ITipoUsuarioDAO tipoUsuarioDAO = new TipoUsuarioDAO();
		return tipoUsuarioDAO.eliminar(id);
	}

	@Override
	public boolean actualizar(TipoUsuario obj) {
		//ITipoUsuarioDAO tipoUsuarioDAO = new TipoUsuarioDAO();
		return tipoUsuarioDAO.actualizar(obj);
	}

	@Override
	public List<TipoUsuario> lista() {
		//ITipoUsuarioDAO tipoUsuarioDAO = new TipoUsuarioDAO();
		return tipoUsuarioDAO.lista();
	}
	@Override
	public TipoUsuario TipoUsuarioPorID(int id) {
		// TODO Auto-generated method stub
		return tipoUsuarioDAO.TipoUsuarioPorID(id);
	}

}
