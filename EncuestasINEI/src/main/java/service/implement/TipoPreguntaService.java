package service.implement;

import java.util.List;

import dao.ITipoPreguntaDAO;
import dao.implement.TipoPreguntaDAO;
import model.TipoPregunta;
import service.ITipoPreguntaService;

public class TipoPreguntaService implements ITipoPreguntaService {

	private ITipoPreguntaDAO tipoPreguntaDAO;
	public TipoPreguntaService() {
		tipoPreguntaDAO = new TipoPreguntaDAO();
	}
	@Override
	public boolean crear(TipoPregunta obj) {
		//ITipoPreguntaDAO tipoPreguntaDAO = new TipoPreguntaDAO();
		return tipoPreguntaDAO.crear(obj);
	}

	@Override
	public boolean eliminar(int id) {
		//ITipoPreguntaDAO tipoPreguntaDAO = new TipoPreguntaDAO();
		return tipoPreguntaDAO.eliminar(id);
	}

	@Override
	public boolean actualizar(TipoPregunta obj) {
		//ITipoPreguntaDAO tipoPreguntaDAO = new TipoPreguntaDAO();
		return tipoPreguntaDAO.actualizar(obj);
	}

	@Override
	public List<TipoPregunta> lista() {
		//ITipoPreguntaDAO tipoPreguntaDAO = new TipoPreguntaDAO();
		return tipoPreguntaDAO.lista();
	}
	@Override
	public TipoPregunta TipoPreguntaPorID(int id) {
		// TODO Auto-generated method stub
		return tipoPreguntaDAO.TipoPreguntaPorID(id);
	}

}
