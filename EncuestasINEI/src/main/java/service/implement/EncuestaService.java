package service.implement;

import java.sql.Date;
import java.util.List;

import dao.IEncuestaDAO;
import dao.implement.EncuestaDAO;
import model.Encuesta;
import service.IEncuestaService;

public class EncuestaService implements IEncuestaService {

	private IEncuestaDAO encuestaDAO;
	public EncuestaService() {
		// TODO Auto-generated constructor stub
		encuestaDAO=new EncuestaDAO();
	}
	
	@Override
	public boolean crear(Encuesta obj) {	
		return encuestaDAO.crear(obj);
	}

	@Override
	public boolean eliminar(int id) {
		
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.eliminar(id);
	}

	@Override
	public boolean actualizar(Encuesta obj) {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.actualizar(obj);
	}

	@Override
	public List<Encuesta> lista() {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.lista();
	}

	@Override
	public List<Encuesta> EncuestasParaUsuarios() {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.lista();
	}

	@Override
	public List<Encuesta> EncuestasRealizadas() {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.EncuestasRealizadas();
	}

	@Override
	public List<Encuesta> EncuestasPorCosto(Double desde, Double hasta, boolean activos, boolean inactivos) {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.EncuestasPorCosto(desde, hasta, activos, inactivos);
	}

	@Override
	public List<Encuesta> TopEncuestas() {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.TopEncuestas();
	}

	@Override
	public List<Encuesta> EncuestasPorFecha(Date desde, Date hasta, boolean activos, boolean inactivos) {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.EncuestasPorFecha(desde, hasta, activos, inactivos);
	}

	@Override
	public List<Encuesta> reporte1() {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.reporte1();
	}

	@Override
	public List<Encuesta> reporte5() {
		//IEncuestaDAO encuestaDAO = new EncuestaDAO();
		return encuestaDAO.reporte5();
	}

	@Override
	public Encuesta EncuestaPorID(int id) {
		// TODO Auto-generated method stub
		return encuestaDAO.EncuestaPorID(id);
	}

	@Override
	public List<Encuesta> EncuestasRespondidasDeUsuarios(int id_usuario) {
		// TODO Auto-generated method stub
		return encuestaDAO.EncuestasRespondidasDeUsuarios(id_usuario);
	}

}
