package service.implement;

import java.util.List;

import dao.IUsuario_EncuestaDAO;
import dao.implement.Usuario_EncuestaDAO;
import model.Usuario_Encuesta;
import service.IUsuario_EncuestaService;

public class Usuario_EncuestaService implements IUsuario_EncuestaService {

	private IUsuario_EncuestaDAO usuario_encuestaDAO;
	public Usuario_EncuestaService() {
		usuario_encuestaDAO = new Usuario_EncuestaDAO();
	}
	@Override
	public boolean crear(Usuario_Encuesta obj) {
		//IUsuario_EncuestaDAO usuario_encuestaDAO = new Usuario_EncuestaDAO();
		return usuario_encuestaDAO.crear(obj);
	}

	@Override
	public boolean eliminar(int id) {
		//IUsuario_EncuestaDAO usuario_encuestaDAO = new Usuario_EncuestaDAO();
		return usuario_encuestaDAO.eliminar(id);
	}

	@Override
	public boolean actualizar(Usuario_Encuesta obj) {
		//IUsuario_EncuestaDAO usuario_encuestaDAO = new Usuario_EncuestaDAO();
		return usuario_encuestaDAO.actualizar(obj);
	}

	@Override
	public List<Usuario_Encuesta> lista() {
		//IUsuario_EncuestaDAO usuario_encuestaDAO = new Usuario_EncuestaDAO();
		return usuario_encuestaDAO.lista();
	}
	@Override
	public int GetIdxUsuarioxEncuesta(int id_usuario, int id_encuesta) {
		// TODO Auto-generated method stub
		return usuario_encuestaDAO.GetIdxUsuarioxEncuesta(id_usuario, id_encuesta);
	}

}
