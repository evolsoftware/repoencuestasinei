package service.implement;

import java.util.List;

import dao.IRespuestaDAO;
import dao.implement.RespuestaDAO;
import model.Respuesta;
import service.IRespuestaService;

public class RespuestaService implements IRespuestaService {

	private IRespuestaDAO respuestaDAO;
	public RespuestaService() {
		respuestaDAO = new RespuestaDAO();
	}
	@Override
	public boolean crear(Respuesta obj) {
		//IRespuestaDAO respuestaDAO = new RespuestaDAO();
		return respuestaDAO.crear(obj);
	}

	@Override
	public boolean eliminar(int id) {
		//IRespuestaDAO respuestaDAO = new RespuestaDAO();
		return respuestaDAO.eliminar(id);
	}

	@Override
	public boolean actualizar(Respuesta obj) {
		//IRespuestaDAO respuestaDAO = new RespuestaDAO();
		return respuestaDAO.actualizar(obj);
	}

	@Override
	public List<Respuesta> lista() {
		//IRespuestaDAO respuestaDAO = new RespuestaDAO();
		return respuestaDAO.lista();
	}

	@Override
	public boolean ResolverEncuesta(List<Respuesta> respuestas) {
		//IRespuestaDAO respuestaDAO = new RespuestaDAO();
		return respuestaDAO.ResolverEncuesta(respuestas);
	}

	@Override
	public List<Respuesta> DetallesEncuestaResuelta(int id_usuario_encuesta) {
		//IRespuestaDAO respuestaDAO = new RespuestaDAO();
		return respuestaDAO.DetallesEncuestaResuelta(id_usuario_encuesta);
	}
	@Override
	public String Texto_respuestaxUExP(int id_usuario_encuesta, int id_pregunta) {
		// TODO Auto-generated method stub
		return respuestaDAO.Texto_respuestaxUExP(id_usuario_encuesta, id_pregunta);
	}

}
