package service.implement;

import java.util.List;

import dao.IUsuarioDAO;
import dao.implement.UsuarioDAO;
import model.Usuario;
import service.IUsuarioService;

public class UsuarioService implements IUsuarioService {

	private IUsuarioDAO usuarioDAO;
	public UsuarioService() {
		usuarioDAO = new UsuarioDAO();
	}
	@Override
	public boolean crear(Usuario obj) {
		//IUsuarioDAO usuarioDAO = new UsuarioDAO();
		return usuarioDAO.crear(obj);
	}

	@Override
	public boolean eliminar(int id) {
		//IUsuarioDAO usuarioDAO = new UsuarioDAO();
		return usuarioDAO.eliminar(id);
	}

	@Override
	public boolean actualizar(Usuario obj) {
		//IUsuarioDAO usuarioDAO = new UsuarioDAO();
		return usuarioDAO.actualizar(obj);
	}

	@Override
	public List<Usuario> lista() {
		//IUsuarioDAO usuarioDAO = new UsuarioDAO();
		return usuarioDAO.lista();
	}

	@Override
	public List<Usuario> UsuariosPorEncuesta(int id_encuesta) {
		//IUsuarioDAO usuarioDAO = new UsuarioDAO();
		return usuarioDAO.UsuariosPorEncuesta(id_encuesta);
	}

	@Override
	public Usuario login(String username, String password) {
		//IUsuarioDAO usuarioDAO = new UsuarioDAO();
	    return usuarioDAO.login(username, password);	
	}
	@Override
	public boolean EditarUsuarioLogueado(Usuario obj) {
		//IUsuarioDAO usuarioDAO = new UsuarioDAO();
	    return usuarioDAO.EditarUsuarioLogueado(obj);	
	}
	@Override
	public Usuario UsuarioPorId(int id_usuario) {
		// TODO Auto-generated method stub
		return usuarioDAO.UsuarioPorId(id_usuario);
	}
	@Override
	public List<Usuario> UsuariosYTotalEncuestasRespondidas() {
		// TODO Auto-generated method stub
		return usuarioDAO.UsuariosYTotalEncuestasRespondidas();
	}

}
