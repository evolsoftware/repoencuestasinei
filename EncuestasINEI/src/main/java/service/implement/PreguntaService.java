package service.implement;

import java.util.List;

import dao.IPreguntaDAO;
import dao.implement.PreguntaDAO;
import model.Pregunta;
import service.IPreguntaService;

public class PreguntaService implements IPreguntaService {

	private IPreguntaDAO preguntaDAO;
	public PreguntaService(){
		preguntaDAO=new PreguntaDAO();
	}
	@Override
	public boolean crear(Pregunta obj) {
		//IPreguntaDAO preguntaDAO = new PreguntaDAO();
		return preguntaDAO.crear(obj);
	}

	@Override
	public boolean eliminar(int id) {
		//IPreguntaDAO preguntaDAO = new PreguntaDAO();
		return preguntaDAO.eliminar(id);
	}

	@Override
	public boolean actualizar(Pregunta obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Pregunta> lista() {
		//IPreguntaDAO preguntaDAO = new PreguntaDAO();
		return preguntaDAO.lista();
	}
	@Override
	public List<Pregunta> PreguntasEncuesta(int id_encuesta) {
		// TODO Auto-generated method stub
		return preguntaDAO.PreguntasEncuesta(id_encuesta);
	}

}
