package service;

import java.util.List;

import model.TipoPregunta;

public interface ITipoPreguntaService {
	public boolean crear(TipoPregunta obj);
	public boolean eliminar(int id);
	public boolean actualizar(TipoPregunta obj);
	public List<TipoPregunta> lista();
	public TipoPregunta TipoPreguntaPorID(int id);
}
