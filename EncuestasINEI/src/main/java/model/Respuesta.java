package model;

public class Respuesta {
	private int id;
	private int usuario_encuestaID;
	private int preguntaID;
	private String texto;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUsuario_encuestaID() {
		return usuario_encuestaID;
	}
	public void setUsuario_encuestaID(int usuario_encuestaID) {
		this.usuario_encuestaID = usuario_encuestaID;
	}
	public int getPreguntaID() {
		return preguntaID;
	}
	public void setPreguntaID(int preguntaID) {
		this.preguntaID = preguntaID;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
}
