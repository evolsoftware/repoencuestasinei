package model;

import java.sql.Date;

public class Encuesta {
	private int id;
	private String nombre;
	private String descripcion;
	private Date fecha;
	private Double costo;
	private boolean alcanceNacional;
	private boolean estado;
	private int totalResueltos; //Numero de veces que fue resuelta
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Double getCosto() {
		return costo;
	}
	public void setCosto(Double costo) {
		this.costo = costo;
	}
	public boolean getAlcanceNacional() {
		return alcanceNacional;
	}
	public void setAlcanceNacional(boolean alcanceNacional) {
		this.alcanceNacional = alcanceNacional;
	}
	public boolean getEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public int getTotalResueltos() {
		return totalResueltos;
	}
	public void setTotalResueltos(int totalResueltos) {
		this.totalResueltos = totalResueltos;
	}
}
