package model;

public class Usuario_Encuesta {
	private int id;
	private int usuarioID;
	private int encuestaID;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUsuarioID() {
		return usuarioID;
	}
	public void setUsuarioID(int usuarioID) {
		this.usuarioID = usuarioID;
	}
	public int getEncuestaID() {
		return encuestaID;
	}
	public void setEncuestaID(int encuestaID) {
		this.encuestaID = encuestaID;
	}
}
