package model;

public class Pregunta {
	private int id;
	private String descripcion;
	private int orden;
	private int tipoPreguntaID;
	private int encuestaID;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getTipoPreguntaID() {
		return tipoPreguntaID;
	}
	public void setTipoPreguntaID(int tipoPreguntaID) {
		this.tipoPreguntaID = tipoPreguntaID;
	}
	public int getEncuestaID() {
		return encuestaID;
	}
	public void setEncuestaID(int encuestaID) {
		this.encuestaID = encuestaID;
	}
}
