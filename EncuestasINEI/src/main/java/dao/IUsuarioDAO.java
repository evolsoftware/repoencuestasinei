package dao;

import java.util.List;

import model.Usuario;

public interface IUsuarioDAO extends IDAO<Usuario>{
	public List<Usuario> UsuariosPorEncuesta(int id_encuesta);
	public Usuario login(String username, String password);
	public boolean EditarUsuarioLogueado(Usuario obj);
	public Usuario UsuarioPorId(int id_usuario);
	public List<Usuario> UsuariosYTotalEncuestasRespondidas();
}
