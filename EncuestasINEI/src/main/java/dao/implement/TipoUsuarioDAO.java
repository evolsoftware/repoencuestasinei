package dao.implement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.Conexion;
import dao.ITipoUsuarioDAO;
import model.TipoUsuario;

public class TipoUsuarioDAO implements ITipoUsuarioDAO {

	@Override
	public boolean crear(TipoUsuario obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "INSERT INTO tipousuario (Nombre, Descripcion) "+
			"VALUES ('" + obj.getNombre() + "', '"+obj.getDescripcion()+"') ";
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public boolean eliminar(int id) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "delete from tipousuario where id=" + id;
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public boolean actualizar(TipoUsuario obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "update tipousuario set Nombre='" + obj.getNombre() +"'" + 
			", Descripcion='"+obj.getDescripcion()+"'" +
					" where ID="+obj.getId();
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public List<TipoUsuario> lista() {
		// TODO Auto-generated method stub
		List<TipoUsuario> tiposUsuario = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select * from tipousuario";	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				TipoUsuario objTipoUsuario = new TipoUsuario();
				objTipoUsuario.setId(rs.getInt("ID"));
				objTipoUsuario.setNombre(rs.getString("Nombre"));
				objTipoUsuario.setDescripcion(rs.getString("Descripcion"));
				tiposUsuario.add(objTipoUsuario);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tiposUsuario;
	}

	@Override
	public TipoUsuario TipoUsuarioPorID(int id) {
		// TODO Auto-generated method stub
		TipoUsuario objTipoUsuario = new TipoUsuario();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* from tipousuario where ID=" + id;	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				objTipoUsuario.setId(rs.getInt("ID"));
				objTipoUsuario.setNombre(rs.getString("Nombre"));
				objTipoUsuario.setDescripcion(rs.getString("Descripcion"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return objTipoUsuario;
	}

}
