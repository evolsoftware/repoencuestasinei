package dao.implement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import dao.Conexion;
import dao.IRespuestaDAO;
import model.Respuesta;

public class RespuestaDAO implements IRespuestaDAO {

	@Override
	public boolean crear(Respuesta obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "INSERT INTO respuesta (UsuarioEncuestaID, PreguntaID, Texto) "+
			"VALUES ('" + obj.getUsuario_encuestaID() + "', "+
					"'"+ obj.getPreguntaID() + "', "+
					"'"+ obj.getTexto() + ") ";
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public boolean eliminar(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean actualizar(Respuesta obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Respuesta> lista() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean ResolverEncuesta(List<Respuesta> respuestas) {
		// TODO Auto-generated method stub
		for(int i=0;i<respuestas.size();i++) {
			crear(respuestas.get(i));
		}
		return false;
	}

	@Override
	public List<Respuesta> DetallesEncuestaResuelta(int id_usuario_encuesta) {
		// TODO Auto-generated method stub
		List<Respuesta> respuestas=new ArrayList<>();
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "select* from respuesta where Usuario_EncuestaID="+id_usuario_encuesta;	
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				Respuesta objRespuesta=new Respuesta();
				objRespuesta.setId(rs.getInt("ID"));
				objRespuesta.setPreguntaID(rs.getInt("PreguntaID"));
				objRespuesta.setTexto(rs.getString("Texto"));
				objRespuesta.setUsuario_encuestaID(rs.getInt("Usuario_EncuestaID"));
				respuestas.add(objRespuesta);
			}
			respuestas.sort(Comparator.comparing(Respuesta::getPreguntaID));
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respuestas;
	}

	@Override
	public String Texto_respuestaxUExP(int id_usuario_encuesta, int id_pregunta) {
		// TODO Auto-generated method stub
		String text="No encontrado";
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "select r.Texto " + 
					"from respuesta r " + 
					"where r.Usuario_EncuestaID="+id_usuario_encuesta+" and r.PreguntaID="+id_pregunta;
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				text=rs.getString("Texto");
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return text;
	}

}
