package dao.implement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.Conexion;
import dao.IUsuarioDAO;
import model.Usuario;

public class UsuarioDAO implements IUsuarioDAO {

	@Override
	public boolean crear(Usuario obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "INSERT INTO usuario (Nombre, Username, Password, Email, TipoUsuarioID) "+
			"VALUES ('" + obj.getNombre() + "', "+
					"'"+ obj.getUsername() + "', "+
					"'"+ obj.getPassword() + "', "+
					"'"+ obj.getEmail() + "', "+
					obj.getTipoUsuarioID() + ") ";
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public boolean eliminar(int id) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "delete from usuario where id=" + id;
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean actualizar(Usuario obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "update usuario set Nombre='" + obj.getNombre() +
							"', Username='" + obj.getUsername() +
							"', Password='" + obj.getPassword() +
							"', Email='" + obj.getEmail() +
							"', TipoUsuarioID=" + obj.getTipoUsuarioID() +
							" where ID="+obj.getId();
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public List<Usuario> lista() {
		// TODO Auto-generated method stub
		List<Usuario> usuarios = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* from usuario";	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Usuario objUsuario = new Usuario();
				objUsuario.setId(rs.getInt("ID"));
				objUsuario.setNombre(rs.getString("Nombre"));
				objUsuario.setUsername(rs.getString("Username"));
				objUsuario.setPassword(rs.getString("Password"));
				objUsuario.setEmail(rs.getString("Email"));
				objUsuario.setTipoUsuarioID(rs.getInt("TipoUsuarioID"));
				usuarios.add(objUsuario);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return usuarios;
	}

	@Override
	public List<Usuario> UsuariosPorEncuesta(int id_encuesta) {
		// TODO Auto-generated method stub
		List<Usuario> usuarios = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select distinct u.* " + 
					"from usuario u, usuario_encuesta ue " + 
					"where u.ID = ue.UsuarioID and ue.EncuestaID=" + id_encuesta +
					" order by u.Nombre";
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Usuario objUsuario = new Usuario();
				objUsuario.setId(rs.getInt("ID"));
				objUsuario.setNombre(rs.getString("Nombre"));
				objUsuario.setUsername(rs.getString("Username"));
				objUsuario.setPassword(rs.getString("Password"));
				objUsuario.setEmail(rs.getString("Email"));
				objUsuario.setTipoUsuarioID(rs.getInt("TipoUsuarioID"));
				usuarios.add(objUsuario);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return usuarios;
	}

	@Override
	public Usuario login(String username, String password) {
		// TODO Auto-generated method stub
		Usuario objUsuario = new Usuario();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* from usuario where Username='" + username + "'";	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				objUsuario.setId(rs.getInt("ID"));
				objUsuario.setNombre(rs.getString("Nombre"));
				objUsuario.setUsername(rs.getString("Username"));
				objUsuario.setPassword(rs.getString("Password"));
				objUsuario.setEmail(rs.getString("Email"));
				objUsuario.setTipoUsuarioID(rs.getInt("TipoUsuarioID"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return objUsuario;
	}

	@Override
	public boolean EditarUsuarioLogueado(Usuario obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "update usuario set Nombre='" + obj.getNombre() +
							"', Username='" + obj.getUsername() +
							"', Password='" + obj.getPassword() +
							"', Email='" + obj.getEmail() +
							"' where ID="+obj.getId();
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Usuario UsuarioPorId(int id_usuario) {
		// TODO Auto-generated method stub
		Usuario objUsuario = new Usuario();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* from usuario where ID=" + id_usuario;	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				objUsuario.setId(rs.getInt("ID"));
				objUsuario.setNombre(rs.getString("Nombre"));
				objUsuario.setUsername(rs.getString("Username"));
				objUsuario.setPassword(rs.getString("Password"));
				objUsuario.setEmail(rs.getString("Email"));
				objUsuario.setTipoUsuarioID(rs.getInt("TipoUsuarioID"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return objUsuario;
	}

	@Override
	public List<Usuario> UsuariosYTotalEncuestasRespondidas() {
		// TODO Auto-generated method stub
		List<Usuario> usuarios = new ArrayList<Usuario>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select  u.*, count(u.ID) as 'Total' " + 
					"from usuario u, usuario_encuesta ue " + 
					"where u.ID=ue.UsuarioID " + 
					"group by u.ID " + 
					"order by u.Nombre";
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Usuario objUsuario = new Usuario();
				objUsuario.setId(rs.getInt("ID"));
				objUsuario.setNombre(rs.getString("Nombre"));
				objUsuario.setUsername(rs.getString("Username"));
				objUsuario.setPassword(rs.getString("Password"));
				objUsuario.setEmail(rs.getString("Email"));
				objUsuario.setTipoUsuarioID(rs.getInt("TipoUsuarioID"));
				objUsuario.setEncuestasRespondidas(rs.getInt("Total"));
				usuarios.add(objUsuario);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return usuarios;
	}

}
