package dao.implement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import dao.Conexion;
import dao.IPreguntaDAO;
import model.Encuesta;
import model.Pregunta;

public class PreguntaDAO implements IPreguntaDAO {

	@Override
	public boolean crear(Pregunta obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean eliminar(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean actualizar(Pregunta obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Pregunta> lista() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pregunta> PreguntasEncuesta(int id_encuesta) {
		// TODO Auto-generated method stub
		List<Pregunta> preguntas = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select p.ID, p.Descripcion, p.Orden, p.TipoPreguntaID " + 
					"from pregunta p, encuesta e " + 
					"where e.ID=p.EncuestaID and e.ID="+id_encuesta;	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Pregunta objPregunta= new Pregunta();
				objPregunta.setId(rs.getInt("ID"));
				objPregunta.setDescripcion(rs.getString("Descripcion"));
				objPregunta.setOrden(rs.getInt("Orden"));
				objPregunta.setTipoPreguntaID(rs.getInt("TipoPreguntaID"));
				preguntas.add(objPregunta);
			}
			preguntas.sort(Comparator.comparing(Pregunta::getOrden));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return preguntas;
	}
	
}
