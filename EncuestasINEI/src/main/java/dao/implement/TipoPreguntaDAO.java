package dao.implement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.Conexion;
import dao.ITipoPreguntaDAO;
import model.TipoPregunta;

public class TipoPreguntaDAO implements ITipoPreguntaDAO {

	@Override
	public boolean crear(TipoPregunta obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "INSERT INTO tipopregunta (Nombre) "+
			"VALUES ('" + obj.getNombre() + "') ";
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public boolean eliminar(int id) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "delete from tipopregunta where id=" + id;
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public boolean actualizar(TipoPregunta obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "update tipopregunta set Nombre='" + obj.getNombre() +"'" + 
					" where ID="+obj.getId();
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public List<TipoPregunta> lista() {
		// TODO Auto-generated method stub
		List<TipoPregunta> tiposPreguntas = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select * from tipopregunta";	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				TipoPregunta objTipoPregunta = new TipoPregunta();
				objTipoPregunta.setId(rs.getInt("ID"));
				objTipoPregunta.setNombre(rs.getString("Nombre"));
				tiposPreguntas.add(objTipoPregunta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tiposPreguntas;
	}

	@Override
	public TipoPregunta TipoPreguntaPorID(int id) {
		// TODO Auto-generated method stub
		TipoPregunta objTipoPregunta = new TipoPregunta();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* from tipopregunta where ID=" + id;	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				objTipoPregunta.setId(rs.getInt("ID"));
				objTipoPregunta.setNombre(rs.getString("Nombre"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return objTipoPregunta;
	}

}
