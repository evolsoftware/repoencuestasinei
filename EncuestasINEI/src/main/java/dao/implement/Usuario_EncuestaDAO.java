package dao.implement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import dao.Conexion;
import dao.IUsuario_EncuestaDAO;
import model.Encuesta;
import model.Usuario_Encuesta;

public class Usuario_EncuestaDAO implements IUsuario_EncuestaDAO {

	@Override
	public boolean crear(Usuario_Encuesta obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean eliminar(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean actualizar(Usuario_Encuesta obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Usuario_Encuesta> lista() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int GetIdxUsuarioxEncuesta(int id_usuario, int id_encuesta) {
		// TODO Auto-generated method stub
		Usuario_Encuesta objUsuarioEncuesta=new Usuario_Encuesta();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* "
					+ "from usuario_encuesta ue "
					+ "where ue.UsuarioID="+id_usuario+" and ue.EncuestaID="+id_encuesta;
			ResultSet rs = stm.executeQuery(query);
			while(rs.next()) {
				objUsuarioEncuesta.setId(rs.getInt("ID"));
				objUsuarioEncuesta.setEncuestaID(rs.getInt("EncuestaID"));
				objUsuarioEncuesta.setUsuarioID(rs.getInt("UsuarioID"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objUsuarioEncuesta.getId();
	}

}
