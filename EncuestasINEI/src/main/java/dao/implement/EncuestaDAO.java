package dao.implement;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.Conexion;
import dao.IEncuestaDAO;
import model.Encuesta;

public class EncuestaDAO implements IEncuestaDAO {

	@Override
	public boolean crear(Encuesta obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "INSERT INTO encuesta (Nombre, Descripcion, Fecha, Costo, AlcanceNacional, Estado) "+
			"VALUES ('" + obj.getNombre() + "', "+
					"'"+ obj.getDescripcion() + "', "+
					"'"+ obj.getFecha() + "', "+
					""+ obj.getCosto() + ", "+
					""+ ((obj.getAlcanceNacional())?1:0) + ", "+
					""+ ((obj.getEstado())?1:0) + ") ";
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public boolean eliminar(int id) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "delete from encuesta where id=" + id;
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public boolean actualizar(Encuesta obj) {
		// TODO Auto-generated method stub
		Connection c = Conexion.conectar();
		try {
			Statement stmt = c.createStatement();
			String query = "update encuesta set Nombre='" + obj.getNombre() +
							"', Descripcion='" + obj.getDescripcion() +
							"', Fecha='" + obj.getFecha() +
							"', Costo='" + obj.getCosto() +
							"', AlcanceNacional=" + ((obj.getAlcanceNacional())?1:0) +
							", Estado=" + ((obj.getEstado())?1:0) +
							" where ID="+obj.getId();
			stmt.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return false;
	}

	@Override
	public List<Encuesta> lista() {
		// TODO Auto-generated method stub
		List<Encuesta> encuestas = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select * from encuesta";	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Encuesta objEncuesta = new Encuesta();
				objEncuesta.setId(rs.getInt("ID"));
				objEncuesta.setNombre(rs.getString("Nombre"));
				objEncuesta.setDescripcion(rs.getString("Descripcion"));
				objEncuesta.setFecha(rs.getDate("Fecha"));
				objEncuesta.setCosto(rs.getDouble("Costo"));
				objEncuesta.setAlcanceNacional(rs.getBoolean("AlcanceNacional"));
				objEncuesta.setEstado(rs.getBoolean("Estado"));
				encuestas.add(objEncuesta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encuestas;
	}

	@Override
	public List<Encuesta> EncuestasParaUsuarios() {
		// TODO Auto-generated method stub
		List<Encuesta> encuestas = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* from encuesta where Estado=1";	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Encuesta objEncuesta = new Encuesta();
				objEncuesta.setId(rs.getInt("ID"));
				objEncuesta.setNombre(rs.getString("Nombre"));
				objEncuesta.setDescripcion(rs.getString("Descripcion"));
				objEncuesta.setFecha(rs.getDate("Fecha"));
				objEncuesta.setCosto(rs.getDouble("Costo"));
				objEncuesta.setAlcanceNacional(rs.getBoolean("AlcanceNacional"));
				objEncuesta.setEstado(rs.getBoolean("Estado"));
				encuestas.add(objEncuesta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encuestas;
	}

	@Override
	public List<Encuesta> EncuestasRealizadas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Encuesta> EncuestasPorCosto(Double desde, Double hasta, boolean activos, boolean inactivos) {
		// TODO Auto-generated method stub
		List<Encuesta> encuestas = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* " +  
					"from encuesta e " +
					"where e.Costo>=" + desde + " and e.Costo<="+ hasta + " "+
					((activos && !inactivos)?"and e.Estado=1":(!activos && inactivos)?"and e.Estado=0":"");
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Encuesta objEncuesta = new Encuesta();
				objEncuesta.setId(rs.getInt("ID"));
				objEncuesta.setNombre(rs.getString("Nombre"));
				objEncuesta.setDescripcion(rs.getString("Descripcion"));
				objEncuesta.setFecha(rs.getDate("Fecha"));
				objEncuesta.setCosto(rs.getDouble("Costo"));
				objEncuesta.setAlcanceNacional(rs.getBoolean("AlcanceNacional"));
				objEncuesta.setEstado(rs.getBoolean("Estado"));
				encuestas.add(objEncuesta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encuestas;
	}

	@Override
	public List<Encuesta> TopEncuestas() {
		// TODO Auto-generated method stub
		List<Encuesta> encuestas = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select e.*,count(e.ID) as 'Total' " + 
					"from encuesta e, usuario_encuesta ue " + 
					"where e.ID=ue.EncuestaID " + 
					"group by e.ID " + 
					"order by count(e.ID) desc";
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Encuesta objEncuesta = new Encuesta();
				objEncuesta.setId(rs.getInt("ID"));
				objEncuesta.setNombre(rs.getString("Nombre"));
				objEncuesta.setDescripcion(rs.getString("Descripcion"));
				objEncuesta.setFecha(rs.getDate("Fecha"));
				objEncuesta.setCosto(rs.getDouble("Costo"));
				objEncuesta.setAlcanceNacional(rs.getBoolean("AlcanceNacional"));
				objEncuesta.setEstado(rs.getBoolean("Estado"));
				objEncuesta.setTotalResueltos(rs.getInt("Total"));
				encuestas.add(objEncuesta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encuestas;
	}

	@Override
	public List<Encuesta> EncuestasPorFecha(Date desde, Date hasta, boolean activos, boolean inactivos) {
		// TODO Auto-generated method stub
		List<Encuesta> encuestas = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* " +  
					"from encuesta e " +
					"where e.Fecha>='" + desde + "' and e.Fecha<='"+ hasta + "' "+
					((activos && !inactivos)?"and e.Estado=1":(!activos && inactivos)?"and e.Estado=0":"");
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Encuesta objEncuesta = new Encuesta();
				objEncuesta.setId(rs.getInt("ID"));
				objEncuesta.setNombre(rs.getString("Nombre"));
				objEncuesta.setDescripcion(rs.getString("Descripcion"));
				objEncuesta.setFecha(rs.getDate("Fecha"));
				objEncuesta.setCosto(rs.getDouble("Costo"));
				objEncuesta.setAlcanceNacional(rs.getBoolean("AlcanceNacional"));
				objEncuesta.setEstado(rs.getBoolean("Estado"));
				encuestas.add(objEncuesta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encuestas;
	}

	@Override
	public List<Encuesta> reporte1() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Encuesta> reporte5() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Encuesta EncuestaPorID(int id) {
		// TODO Auto-generated method stub
		Encuesta objEncuesta = new Encuesta();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select* from encuesta where ID=" + id;	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				objEncuesta.setId(rs.getInt("ID"));
				objEncuesta.setNombre(rs.getString("Nombre"));
				objEncuesta.setDescripcion(rs.getString("Descripcion"));
				objEncuesta.setFecha(rs.getDate("Fecha"));
				objEncuesta.setCosto(rs.getDouble("Costo"));
				objEncuesta.setAlcanceNacional(rs.getBoolean("AlcanceNacional"));
				objEncuesta.setEstado(rs.getBoolean("Estado"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return objEncuesta;
	}

	@Override
	public List<Encuesta> EncuestasRespondidasDeUsuarios(int id_usuario) {
		// TODO Auto-generated method stub
		List<Encuesta> encuestas = new ArrayList<>();
		Connection c = Conexion.conectar();
		Statement stm;
		try {
			stm = c.createStatement();
			String query = "select e.ID, e.Nombre, e.Descripcion, e.Fecha " + 
					"from encuesta e, usuario_encuesta ue " + 
					"where ue.UsuarioID="+id_usuario+" and e.ID=ue.EncuestaID";	
			ResultSet rs = stm.executeQuery(query);
			
			while(rs.next()) {
				Encuesta objEncuesta = new Encuesta();
				objEncuesta.setId(rs.getInt("ID"));
				objEncuesta.setNombre(rs.getString("Nombre"));
				objEncuesta.setDescripcion(rs.getString("Descripcion"));
				objEncuesta.setFecha(rs.getDate("Fecha"));
				encuestas.add(objEncuesta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encuestas;
	}

}
