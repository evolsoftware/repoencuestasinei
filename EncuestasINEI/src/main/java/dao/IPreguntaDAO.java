package dao;

import java.util.List;

import model.Pregunta;

public interface IPreguntaDAO extends IDAO<Pregunta>{
	public List<Pregunta> PreguntasEncuesta(int id_encuesta);
}
