package dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class Conexion {
	public static Connection conectar() {
		
		Connection conexion = null;		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/encuestasinei",
					"root",
					"root"
					);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		return conexion;
		
		/*
		Connection conexion = null;
		//InputStream inputStream = Conexion.class.getClassLoader().getResourceAsStream("bd.properties");
		//Properties properties = new Properties();
		
		try {
		//	properties.load(inputStream);
			
			Class.forName(properties.getProperty("driver"));
			conexion = DriverManager.getConnection(
					properties.getProperty("url"),
					properties.getProperty("user"),
					properties.getProperty("password")
					);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		return conexion;*/
	}
}