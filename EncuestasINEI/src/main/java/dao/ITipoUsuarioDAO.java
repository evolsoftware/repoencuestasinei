package dao;

import model.TipoUsuario;

public interface ITipoUsuarioDAO extends IDAO<TipoUsuario>{
	public TipoUsuario TipoUsuarioPorID(int id);
}
