package dao;

import java.util.List;

import model.Respuesta;

public interface IRespuestaDAO extends IDAO<Respuesta>{
	public boolean ResolverEncuesta(List<Respuesta> respuestas);
	public List<Respuesta> DetallesEncuestaResuelta(int id_usuario_encuesta);
	public String Texto_respuestaxUExP(int id_usuario_encuesta, int id_pregunta);
}
