package dao;

import java.util.List;

public interface IDAO<T> {
	public boolean crear(T obj);
	public boolean eliminar(int id);
	public boolean actualizar(T obj);
	public List<T> lista(); 
}
