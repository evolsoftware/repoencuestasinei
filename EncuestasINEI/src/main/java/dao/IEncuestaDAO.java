package dao;

import java.sql.Date;
import java.util.List;

import model.Encuesta;

public interface IEncuestaDAO extends IDAO<Encuesta> {
	public List<Encuesta> EncuestasParaUsuarios();
	public List<Encuesta> EncuestasRealizadas();
	public List<Encuesta> EncuestasPorCosto(Double desde, Double hasta, boolean activos, boolean inactivos);
	public List<Encuesta> TopEncuestas();
	public List<Encuesta> EncuestasPorFecha(Date desde, Date hasta, boolean activos, boolean inactivos);
	public List<Encuesta> reporte1();
	public List<Encuesta> reporte5();
	public Encuesta EncuestaPorID(int id);
	public List<Encuesta> EncuestasRespondidasDeUsuarios(int id_usuario);
}
