package dao;

import model.TipoPregunta;

public interface ITipoPreguntaDAO extends IDAO<TipoPregunta>{
	public TipoPregunta TipoPreguntaPorID(int id);
}
