package dao;

import model.Usuario_Encuesta;

public interface IUsuario_EncuestaDAO extends IDAO<Usuario_Encuesta>{
	public int GetIdxUsuarioxEncuesta(int id_usuario, int id_encuesta);
}
