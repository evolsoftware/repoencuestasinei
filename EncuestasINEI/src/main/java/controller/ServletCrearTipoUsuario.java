package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.TipoUsuario;
import service.ITipoUsuarioService;
import service.implement.TipoUsuarioService;

/**
 * Servlet implementation class ServletCrearTipoUsuario
 */
@WebServlet("/ServletCrearTipoUsuario")
public class ServletCrearTipoUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCrearTipoUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String txtNombre = request.getParameter("txtNombre");
		String txtDescripcion = request.getParameter("txtDescripcion");
		int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
		
		TipoUsuario objTipoUsuario = new TipoUsuario();
		objTipoUsuario.setNombre(txtNombre);
		objTipoUsuario.setDescripcion(txtDescripcion);
		ITipoUsuarioService tipoUsuarioService = new TipoUsuarioService();
		tipoUsuarioService.crear(objTipoUsuario);
		
		response.sendRedirect("administrador_tipos_usuarios.jsp?id_administrador=" + id_administrador);
	}

}
