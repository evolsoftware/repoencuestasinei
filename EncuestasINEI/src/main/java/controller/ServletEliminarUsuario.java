package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.IEncuestaService;
import service.IUsuarioService;
import service.implement.EncuestaService;
import service.implement.UsuarioService;

/**
 * Servlet implementation class ServletEliminarUsuario
 */
@WebServlet("/ServletEliminarUsuario")
public class ServletEliminarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEliminarUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
		int id_usuario = Integer.valueOf(request.getParameter("id_usuario"));
		IUsuarioService usuarioService = new UsuarioService();
		if(usuarioService.eliminar(id_usuario)) {
			response.sendRedirect("administrador_usuarios.jsp?id_administrador=" + id_administrador);
		}
		else {
			response.sendRedirect("administrador_error_eliminar_usuario.jsp?id_administrador=" + id_administrador);
		}
	}

}
