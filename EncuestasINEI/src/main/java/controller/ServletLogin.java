package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Usuario;
import service.IUsuarioService;
import service.implement.UsuarioService;

/**
 * Servlet implementation class ServletLogin
 */
@WebServlet("/ServletLogin")
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String txtUsername = request.getParameter("txtUsername");
		String txtContrase�a = request.getParameter("txtContrase�a");
		
		IUsuarioService usuarioService = new UsuarioService();
		Usuario objUsuario = usuarioService.login(txtUsername, txtContrase�a);
		Boolean usuarioValido = false;
		request.setAttribute("usuarioValido", usuarioValido);
		if(objUsuario.getNombre() != null) {
			if(objUsuario.getPassword().equals(txtContrase�a)) {
				usuarioValido = true;
				request.setAttribute("usuarioValido", usuarioValido);
				switch(objUsuario.getTipoUsuarioID()) {
				case 1: response.sendRedirect("encuestado_encuestas_nuevas.jsp?id_encuestado=" + objUsuario.getId()); break; //Encuestado
				case 2: response.sendRedirect("administrador_usuarios.jsp?id_administrador=" + objUsuario.getId()); break; //Administrador
				default: response.sendRedirect("director_mis_encuestas.jsp?id_director=" + objUsuario.getId()); //Director
				}
			}
			else {
				RequestDispatcher rd=request.getRequestDispatcher("login.jsp");
	            rd.forward(request, response);
			}
		}
		else {
			 RequestDispatcher rd=request.getRequestDispatcher("login.jsp");
             rd.forward(request, response);
		}
	}

}
