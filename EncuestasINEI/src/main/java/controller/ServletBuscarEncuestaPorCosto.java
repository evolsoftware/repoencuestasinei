package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Encuesta;
import service.IEncuestaService;
import service.implement.EncuestaService;

/**
 * Servlet implementation class ServletBuscarEncuestaPorCosto
 */
@WebServlet("/ServletBuscarEncuestaPorCosto")
public class ServletBuscarEncuestaPorCosto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletBuscarEncuestaPorCosto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Double desde = Double.valueOf(request.getParameter("txtDesde"));
		Double hasta = Double.valueOf(request.getParameter("txtHasta"));
		Boolean activos;
		if(request.getParameter("cbActivos") != null) {
			activos = true;
		}
		else {
			activos = false;
		}
		Boolean inactivos;
		if(request.getParameter("cbInactivos") != null ) {
			inactivos = true;
		}
		else {
			inactivos = false;
		}
		
		int id_director = Integer.valueOf(request.getParameter("id_director"));
		IEncuestaService encuestaService = new EncuestaService();
		List<Encuesta> encuestas = encuestaService.EncuestasPorCosto(desde, hasta, activos, inactivos);
		request.setAttribute("encuestas", encuestas);
		RequestDispatcher rd=request.getRequestDispatcher("director_reporte4_encuestas_por_costo.jsp?id_director=" + id_director);
        rd.forward(request, response);
	}

}
