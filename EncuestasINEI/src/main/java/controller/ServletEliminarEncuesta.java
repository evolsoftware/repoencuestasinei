package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.IEncuestaService;
import service.implement.EncuestaService;

/**
 * Servlet implementation class ServletEliminarEncuesta
 */
@WebServlet("/ServletEliminarEncuesta")
public class ServletEliminarEncuesta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEliminarEncuesta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int id_director = Integer.valueOf(request.getParameter("id_director"));
		int id_encuesta = Integer.valueOf(request.getParameter("id_encuesta"));
		IEncuestaService encuestaService = new EncuestaService();
		if(encuestaService.eliminar(id_encuesta)) {
			response.sendRedirect("director_mis_encuestas.jsp?id_director=" + id_director);
		}
		else {
			response.sendRedirect("director_error_eliminar_encuesta.jsp?id_director=" + id_director);
		}
	}

}
