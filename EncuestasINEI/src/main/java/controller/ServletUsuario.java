package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.TipoUsuario;
import model.Usuario;
import service.ITipoUsuarioService;
import service.IUsuarioService;
import service.implement.TipoUsuarioService;
import service.implement.UsuarioService;

/**
 * Servlet implementation class ServletUsuario
 */
@WebServlet("/ServletUsuario")
public class ServletUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String accion = request.getParameter("accion");
		
		
		if(accion.equals("crear")) {
			ITipoUsuarioService iTipoUsuarioService = new TipoUsuarioService();
			List<TipoUsuario> tiposUsuario = iTipoUsuarioService.lista();
			
			request.setAttribute("tiposUsuario", tiposUsuario);
			
			request.getRequestDispatcher("/usuario_crear.jsp").forward(request, response);
		}else if(accion.equals("listar")) {
			
			IUsuarioService usuarioService = new UsuarioService();
			List<Usuario> listado = usuarioService.lista();
			
			request.setAttribute("listadoObj", listado);
			
			request.getRequestDispatcher("/usuario_listado.jsp").forward(request, response);
			
		}else if (accion.equals("eliminar")) {
				String id = request.getParameter("usuario");
				IUsuarioService iUsuarioService = new UsuarioService();
				boolean flag = iUsuarioService.eliminar(Integer.parseInt(id));
				
				if(flag) {
					request.setAttribute("mensaje", "Usuario eliminado");
				}else {
					request.setAttribute("mensaje", "Ocurri� un error, vuelva a intentarlo");
				}
				
				request.getRequestDispatcher("/resultado.jsp").forward(request, response);
		}else {
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nombre = request.getParameter("txtNombre");
		String username = request.getParameter("txtUsername");
		String password = request.getParameter("txtPassword");
		String email = request.getParameter("txtEmail");
		Integer tipoUsuarioID = Integer.valueOf(request.getParameter("cbTipoUsuario"));
		
		Usuario objUsuario = new Usuario();
		objUsuario.setNombre(nombre);
		objUsuario.setUsername(username);
		objUsuario.setPassword(password);
		objUsuario.setEmail(email);
		objUsuario.setTipoUsuarioID(tipoUsuarioID);
		
		IUsuarioService usuarioService = new UsuarioService();
		boolean flag = usuarioService.crear(objUsuario);
		
		if(flag) {
			request.setAttribute("mensaje", "Usuario creado");
		}else {
			request.setAttribute("mensaje", "Ocurri� un error, vuelva a intentarlo");
		}
		
		request.getRequestDispatcher("/resultado.jsp").forward(request, response);
	}

}
