package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Pregunta;
import model.Respuesta;
import model.Usuario_Encuesta;
import service.IPreguntaService;
import service.IRespuestaService;
import service.IUsuario_EncuestaService;
import service.implement.PreguntaService;
import service.implement.RespuestaService;
import service.implement.Usuario_EncuestaService;

/**
 * Servlet implementation class ServletResponderEncuesta
 */
@WebServlet("/ServletResponderEncuesta")
public class ServletResponderEncuesta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletResponderEncuesta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		IUsuario_EncuestaService ues=new Usuario_EncuestaService();
		int id_encuesta=Integer.valueOf(request.getParameter("id_encuesta"));
		int id_encuestado=Integer.valueOf(request.getParameter("id_encuestado"));
		
		IPreguntaService ps=new PreguntaService();
		List<Pregunta> preguntas=ps.PreguntasEncuesta(id_encuesta);
		List<Respuesta> respuestas=new ArrayList<>();
		
		Usuario_Encuesta objUsuario_encuesta=new Usuario_Encuesta();
		objUsuario_encuesta.setEncuestaID(id_encuesta);
		objUsuario_encuesta.setUsuarioID(id_encuestado);
		ues.crear(objUsuario_encuesta);
		int id_usuario_encuesta=ues.GetIdxUsuarioxEncuesta(id_encuestado, id_encuesta);
		if(id_usuario_encuesta>0) {
			objUsuario_encuesta.setId(id_usuario_encuesta);
			
			for(int i=0;i<preguntas.size();i++) {
				int id_pregunta=Integer.valueOf(request.getParameter("idp"));
				String texto=request.getParameter("txtRespuesta"+i);
				
				Respuesta objRespuesta=new Respuesta();
				objRespuesta.setUsuario_encuestaID(objUsuario_encuesta.getId());
				objRespuesta.setPreguntaID(id_pregunta);
				objRespuesta.setTexto(texto);
				
				respuestas.add(objRespuesta);
			}
			
			IRespuestaService rs=new RespuestaService();
			rs.ResolverEncuesta(respuestas);
		}
		response.sendRedirect("encuestado_encuestas_nuevas.jsp?id_encuestado=" + id_encuestado);
	}

}
