package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.ITipoPreguntaService;
import service.implement.TipoPreguntaService;

/**
 * Servlet implementation class ServletEliminarTipoPregunta
 */
@WebServlet("/ServletEliminarTipoPregunta")
public class ServletEliminarTipoPregunta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEliminarTipoPregunta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
		int id_tipo_pregunta = Integer.valueOf(request.getParameter("id_tipo_pregunta"));
		ITipoPreguntaService tipoPreguntaService = new TipoPreguntaService();
		if(tipoPreguntaService.eliminar(id_tipo_pregunta)) {
			response.sendRedirect("administrador_tipos_preguntas.jsp?id_administrador=" + id_administrador);
		}
		else {
			response.sendRedirect("administrador_error_eliminar_tipo_pregunta.jsp?id_administrador=" + id_administrador);
		}
	}

}
