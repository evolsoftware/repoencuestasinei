package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Usuario;
import service.IUsuarioService;
import service.implement.UsuarioService;

/**
 * Servlet implementation class ServletPerfilAdministrador
 */
@WebServlet("/ServletPerfilAdministrador")
public class ServletPerfilAdministrador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletPerfilAdministrador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		IUsuarioService us=new UsuarioService();
		Usuario objUsuario=new Usuario();
		
		int id_usuario=Integer.valueOf(request.getParameter("id_administrador"));
		int id_tipo_usuario=Integer.valueOf(request.getParameter("id_tipo_usuario"));
		String nombre=request.getParameter("txtNombre");
		String username=request.getParameter("txtUsername");
		String password1=request.getParameter("txtContrase�a");
		String password2=request.getParameter("txtRepetirContrase�a");
		String email=request.getParameter("txtCorreo");
		
		if(password1.equals("")||password2.equals("")||password1.equals(password2)==false) {
			Boolean validesContrase�a=false;
			request.setAttribute("validesContrase�a", validesContrase�a);
			RequestDispatcher rd=request.getRequestDispatcher("administrador_perfil.jsp?"+id_usuario);
            rd.forward(request, response);
		}else {
			objUsuario.setId(id_usuario);
			objUsuario.setNombre(nombre);
			objUsuario.setUsername(username);
			objUsuario.setPassword(password1);
			objUsuario.setEmail(email);
			objUsuario.setTipoUsuarioID(id_tipo_usuario);
			us.EditarUsuarioLogueado(objUsuario);
			response.sendRedirect("administrador_tipos_preguntas.jsp?id_administrador="+id_usuario);
		}
	}
}
