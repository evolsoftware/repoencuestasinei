package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Encuesta;
import service.IEncuestaService;
import service.implement.EncuestaService;

/**
 * Servlet implementation class ServletCrearEncuesta
 */
@WebServlet("/ServletCrearEncuesta")
public class ServletCrearEncuesta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCrearEncuesta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String txtNombre = request.getParameter("txtNombre");
		String txtDescripcion = request.getParameter("txtDescripcion");
		Date txtFecha =  Date.valueOf(request.getParameter("txtFecha"));
		Double txtCosto = Double.valueOf(request.getParameter("txtCosto"));

		Boolean alcanceNacional;
		if(request.getParameter("cbAlcanceNacional") != null) {
			alcanceNacional = true;
		}
		else {
			alcanceNacional = false;
		}
		Boolean estado;
		if(request.getParameter("cbEstado") != null ) {
			estado = true;
		}
		else {
			estado = false;
		}
		
		int id_director = Integer.valueOf(request.getParameter("id_director"));
		
		Encuesta objEncuesta = new Encuesta();
		objEncuesta.setNombre(txtNombre);
		objEncuesta.setDescripcion(txtDescripcion);
		objEncuesta.setFecha(txtFecha);
		objEncuesta.setCosto(txtCosto);
		objEncuesta.setAlcanceNacional(alcanceNacional);
		objEncuesta.setEstado(estado);
		
		IEncuestaService encuestaService = new EncuestaService();
		encuestaService.crear(objEncuesta);
		
		response.sendRedirect("director_mis_encuestas.jsp?id_director=" + id_director);
		
	}

}
