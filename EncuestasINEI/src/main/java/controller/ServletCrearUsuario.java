package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Usuario;
import service.IUsuarioService;
import service.implement.UsuarioService;

/**
 * Servlet implementation class ServletCrearUsuario
 */
@WebServlet("/ServletCrearUsuario")
public class ServletCrearUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCrearUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String txtNombre = request.getParameter("txtNombre");
		String txtUsername = request.getParameter("txtUsername");
		String txtPassword =  request.getParameter("txtPassword");
		String txtEmail = request.getParameter("txtEmail");
		int id_tipoUsuario = Integer.valueOf(request.getParameter("sTipoUsuario"));
		int id_administrador = Integer.valueOf(request.getParameter("id_administrador"));
		
		Usuario objUsuario = new Usuario();
		objUsuario.setNombre(txtNombre);
		objUsuario.setUsername(txtUsername);
		objUsuario.setPassword(txtPassword);
		objUsuario.setEmail(txtEmail);
		objUsuario.setTipoUsuarioID(id_tipoUsuario);
		
		IUsuarioService usuarioService = new UsuarioService();
		usuarioService.crear(objUsuario);
		
		response.sendRedirect("administrador_usuarios.jsp?id_administrador=" + id_administrador);
	}

}
