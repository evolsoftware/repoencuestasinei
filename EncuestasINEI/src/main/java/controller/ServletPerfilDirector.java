package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Usuario;
import service.IUsuarioService;
import service.implement.UsuarioService;

/**
 * Servlet implementation class ServletPerfilDirector
 */
@WebServlet("/ServletPerfilDirector")
public class ServletPerfilDirector extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletPerfilDirector() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		IUsuarioService usuarioService = new UsuarioService();
		Usuario objUsuario = new Usuario();
		
		String txtNombre = request.getParameter("txtNombre");
		String txtUsername = request.getParameter("txtUsername");
		String txtContrase�a = request.getParameter("txtContrase�a");
		String txtRepetirContrase�a = request.getParameter("txtRepetirContrase�a");
		String txtCorreo = request.getParameter("txtCorreo");
		int id_director = Integer.valueOf(request.getParameter("id_director"));
		int id_tipo_usuario=Integer.valueOf(request.getParameter("id_tipo_usuario"));
		
		if(txtContrase�a.equals("")||txtRepetirContrase�a.equals("")||txtContrase�a.equals(txtRepetirContrase�a)==false) {
			Boolean validesContrase�a=false;
			request.setAttribute("validesContrase�a", validesContrase�a);
			RequestDispatcher rd=request.getRequestDispatcher("director_perfil.jsp?"+id_director);
            rd.forward(request, response);
		}else {
			objUsuario.setNombre(txtNombre);
			objUsuario.setUsername(txtUsername);
			objUsuario.setPassword(txtContrase�a);
			objUsuario.setEmail(txtCorreo);
			objUsuario.setId(id_director);
			objUsuario.setTipoUsuarioID(id_tipo_usuario);
			
			usuarioService.EditarUsuarioLogueado(objUsuario);
			
			response.sendRedirect("director_mis_encuestas.jsp?id_director=" + id_director);
		}
	}
}
